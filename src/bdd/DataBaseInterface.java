/**
 * @author Marchal Tom
 * Class qui permet de travailler avec la Base de Donnees.
 * Elle permet de ajouter un modele a la base
 * ou de prendre un modele de la base.
 */

package bdd;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;



import Renderer3d.*;

public class DataBaseInterface {
	
	public static int addModele(String name,String filePath,String description){
		boolean succes=false;
		
		if(name==null){
			System.out.println("Il manque le nom");
			return 1;
		}
		if(filePath==null){
			System.out.println("Il manque le chemin");
			return 2;
		}
		if(!name.matches("^\\w+$")){
			System.out.println("le nom comporte des caract�res invalides");
			return 3;
		}
		
		
		Connection con=null;
		try {
		    Class.forName("org.sqlite.JDBC");
		    con = DriverManager.getConnection("jdbc:sqlite:bdd/BddModele.db");
		    Statement stmt = con.createStatement();
		    
		    
		    ResultSet rs = stmt.executeQuery("Select * from modeles where name ='"+name+"'");
		    if(rs.next()){
		    	System.out.println("Il existe un modele avec le meme nom");
		    	con.close();
		    	return 7;
		    }
		    
		    
		   
		    Object3D o3d = new Object3D(filePath);
		    if(o3d.getTriangles()==null){
		    	con.close();
		    	return 10;
		    }
		    
		    
		    Triangle triangles[] = o3d.getTriangles();
		    String blobTriangles="";
		    
		    for(int i=0;i<triangles.length;i++){
		    	blobTriangles+=triangles[i].getIdxVertices()[0]+",";
		    	blobTriangles+=triangles[i].getIdxVertices()[1]+",";
		    	blobTriangles+=triangles[i].getIdxVertices()[2]+";";
		    }

		    Vertex vertices[] = o3d.getVertices();
		    String blobVertices="";
		    
		    for(int i=0;i<vertices.length;i++){
		    	blobVertices+=vertices[i].getX()+",";
		    	blobVertices+=vertices[i].getY()+",";
		    	blobVertices+=vertices[i].getZ()+";";
		    }
		    
		    String blobVoisins="";
		    for(int i =0;i<triangles.length;i++){
		    	blobVoisins+=triangles[i].voisins.get(0)+",";
		    	blobVoisins+=triangles[i].voisins.get(1)+",";
		    	blobVoisins+=triangles[i].voisins.get(2)+";";
		    }
		    
		    
		    
		    stmt.executeUpdate("PRAGMA foreign_keys = ON");
		    String query ="insert into modeles values ('"+name+"','"+description+"','"+triangles.length+"','"+vertices.length+"')"; 
		    stmt.executeUpdate(query);
		    query="insert into triangles values('"+name+"','"+blobTriangles+"')";
		    stmt.executeUpdate(query);
		    query="insert into vertices values('"+name+"','"+blobVertices+"')";
		    stmt.executeUpdate(query);
		    query="insert into voisins values('"+name+"','"+blobVoisins+"')";
		    stmt.executeUpdate(query);
		    succes=true;
		    
		}
		catch (Exception e) {
		    e.printStackTrace();
		}
		finally
		    {
			try{con.close();} catch (Exception e){return -1;}
	    }
		
		if(succes){
			return 0;
		}
		return -1;
	}
	
	public static int addModele(String name,Object3D o3d,String description){
		boolean succes=false;
		
		if(name==null){
			System.out.println("Il manque le nom");
			return 1;
		}
		
		Connection con=null;
		try {
		    Class.forName("org.sqlite.JDBC");
		    con = DriverManager.getConnection("jdbc:sqlite:bdd/BddModele.db");
		    Statement stmt = con.createStatement();
		    
		    //rajouter le truc pour activer les foreign key
		    
		    stmt.executeUpdate("PRAGMA foreign_keys = ON");
		    
		    ResultSet rs = stmt.executeQuery("Select * from modeles where name ='"+name+"'");
		    if(rs.next()){
		    	System.out.println("Il existe un modele avec le meme nom");
		    	con.close();
		    	return 7;
		    }
		    
		    if(o3d.getTriangles()==null){
		    	con.close();
		    	return 10;
		    }
		    
		    
		    
		    
		    Triangle triangles[] = o3d.getTriangles();
		    String blobTriangles="";
		    
		    for(int i=0;i<triangles.length;i++){
		    	blobTriangles+=triangles[i].getIdxVertices()[0]+",";
		    	blobTriangles+=triangles[i].getIdxVertices()[1]+",";
		    	blobTriangles+=triangles[i].getIdxVertices()[2]+";";
		    	
		    
		    }
		    

		    Vertex vertices[] = o3d.getVertices();
		    String blobVertices="";
		    
		    for(int i=0;i<vertices.length;i++){
		    	blobVertices+=vertices[i].getX()+",";
		    	blobVertices+=vertices[i].getY()+",";
		    	blobVertices+=vertices[i].getZ()+";";
		    }
		    
		    String blobVoisins="";
		    for(int i =0;i<triangles.length;i++){
		    	blobVoisins+=triangles[i].voisins.get(0)+",";
		    	blobVoisins+=triangles[i].voisins.get(1)+",";
		    	blobVoisins+=triangles[i].voisins.get(2)+";";
		    }
		    
		    stmt.executeUpdate("PRAGMA foreign_keys = ON");
		    String query ="insert into modeles values ('"+name+"','"+description+"','"+triangles.length+"','"+vertices.length+"')"; 
		    stmt.executeUpdate(query);
		    query="insert into triangles values('"+name+"','"+blobTriangles+"')";
		    stmt.executeUpdate(query);
		    query="insert into vertices values('"+name+"','"+blobVertices+"')";
		    stmt.executeUpdate(query);
		    query="insert into voisins values('"+name+"','"+blobVoisins+"')";
		    stmt.executeUpdate(query);
		    succes=true;
		    
		}
		catch (Exception e) {
		    e.printStackTrace();
		}
		finally
		    {
			try{con.close();} catch (Exception e){return -1;}
	    }
		
		if(succes){
			return 0;
		}
		return -1;
	}
	
	
		
	public static Modele3D getModele(String name){
		Modele3D m= new Modele3D();
		boolean succes=false;
		
		Connection con=null;
		try {
		    Class.forName("org.sqlite.JDBC");
		    con = DriverManager.getConnection("jdbc:sqlite:bdd/BddModele.db");
		    Statement stmt = con.createStatement();
		    
		    ResultSet rs = stmt.executeQuery("Select * from modeles where name ='"+name+"'");
		    if(!rs.next()){
		    	System.out.println("Le modele n'existe pas");
		    	con.close();
		    	return null;
		    }
		    m.setName(name);
		    m.setDescription(rs.getString("description"));
		    m.setNbTriangles(rs.getInt("nbTriangle"));
		    m.setNbVertex(rs.getInt("nbVertex"));
		    
		    rs = stmt.executeQuery("select * from triangles where nameMod='"+name+"'");
		    rs.next();
		    List<Triangle> listTriangle = new ArrayList<Triangle>();
		    String blob=rs.getString("triangles");
		    String split[] = blob.split(";");
		    for(int i=0;i<split.length;i++){
		    	String recupTriangles[]=split[i].split(",");
		    	
		    	int a = Integer.parseInt(recupTriangles[0]);
		    	int b = Integer.parseInt(recupTriangles[1]);
		    	int c = Integer.parseInt(recupTriangles[2]);
		    	
		    	listTriangle.add(new Triangle(a, b, c));
		    }
		    
		    
		    rs = stmt.executeQuery("select * from voisins where nameMod='"+name+"'");
		    rs.next();
		    blob=rs.getString("tri_voisins");
		    split = blob.split(";");
		    
		    for(int i=0;i<split.length;i++){
		    	String recupVoisins[] =split[i].split(",");
		    	
		    	listTriangle.get(i).voisins.add(Integer.parseInt(recupVoisins[0]));
		    	listTriangle.get(i).voisins.add(Integer.parseInt(recupVoisins[1]));
		    	listTriangle.get(i).voisins.add(Integer.parseInt(recupVoisins[2]));
		    }
		    
		    
		    
		    m.setTriangles((Triangle[])listTriangle.toArray(new Triangle[listTriangle.size()]));
		    
		    
		    rs = stmt.executeQuery("select * from vertices where nameMod ='"+name+"'");
		    rs.next();
		    List<Vertex> listVertex = new ArrayList<Vertex>();
		    blob=rs.getString("vertices");
		    split = blob.split(";");
		    
		    for(int i=0;i<split.length;i++){
		    	String recupVertex[]=split[i].split(",");
		    	
		    	double x = Double.parseDouble(recupVertex[0]);
		    	double y = Double.parseDouble(recupVertex[1]);
		    	double z = Double.parseDouble(recupVertex[2]);
		    	
		    	listVertex.add(new Vertex(x, y, z));
		    }
		    m.setVertices((Vertex[])listVertex.toArray(new Vertex[listVertex.size()]));
		    
		    
		    
		    
		    
		    succes=true;
		}
		catch (Exception e) {
		    System.out.println("Erreur : " + e);e.printStackTrace();
		}
		finally
		    {
			try{con.close();} catch (Exception e){
				e.printStackTrace();;
				return null;
			}
	    }
		if(succes){
			return m;
		}
		return null;
	}

	public static int setDescription(String name,String description){
		if(name==null){
			return -1;
		}
		if(description==null){
			description="";
		}
		
		boolean succes=false;
		
		Connection con=null;
		try {
		    Class.forName("org.sqlite.JDBC");
		    con = DriverManager.getConnection("jdbc:sqlite:bdd/BddModele.db");
		    Statement stmt = con.createStatement();
		
		    ResultSet rs = stmt.executeQuery("Select * from modeles where name ='"+name+"'");
		    if(!rs.next()){
		    	System.out.println("Le modele n'existe pas");
		    	con.close();
		    	return -1;
		    }
		    stmt.executeUpdate("PRAGMA foreign_keys = ON");
		    stmt.executeUpdate("Update modeles set description='"+description+"' where name='"+name+"'");
		    
		    succes=true;

		}
		catch (Exception e) {
				    e.printStackTrace();
		}
		finally
		{
			try{con.close();} catch (Exception e){return -1;}
		}
		
		if(succes){
			return 0;
		}
		return -1;
	}
	
	public static int setName(String name,String newName){
		if(name==null){
			return -1;
		}
		if(newName==null){
			return -1;
		}
		if(name.equals(newName)){
			return -1;
		}
		
		boolean succes=false;
		
		Connection con=null;
		try {
		    Class.forName("org.sqlite.JDBC");
		    con = DriverManager.getConnection("jdbc:sqlite:bdd/BddModele.db");
		    Statement stmt = con.createStatement();
		
		    
		    ResultSet rs = stmt.executeQuery("Select * from modeles where name ='"+name+"'");
		    if(!rs.next()){
		    	System.out.println("Le modele n'existe pas");
		    	con.close();
		    	return -1;
		    }
		    
		    rs = stmt.executeQuery("Select * from modeles where name ='"+newName+"'");
		    if(rs.next()){
		    	System.out.println("Ce nom est deja pris");
		    	con.close();
		    	return -1;
		    }
		    stmt.executeUpdate("PRAGMA foreign_keys = ON");
		    stmt.executeUpdate("Update modeles set name='"+newName+"' where name='"+name+"'");
		    
		    succes=true;

		}
		catch (Exception e) {
				    e.printStackTrace();
		}
		finally
		{
			try{con.close();} catch (Exception e){return -1;}
		}
		
		if(succes){
			return 0;
		}
		return -1;
	}
	
	public static List<Modele> getListModele(){
		return getListModeleSearchByName("");
	}
		
	public static List<Modele> getListModeleSearchByName(String s){
		List<Modele> list = new ArrayList<Modele>();
		if(s==null){
			s="";
		}

		Connection con=null;
		try {
		    Class.forName("org.sqlite.JDBC");
		    con = DriverManager.getConnection("jdbc:sqlite:bdd/BddModele.db");
		    Statement stmt = con.createStatement();
		    Modele m=null;
		    ResultSet rs = stmt.executeQuery("Select * from modeles where name like'%"+s+"%'");
		    while(rs.next()){
		    	m = new Modele();
		    	m.setName(rs.getString("name"));
		    	m.setDescription(rs.getString("description"));
		    	m.setNbTriangles(rs.getInt("nbTriangle"));
		    	m.setNbVertex(rs.getInt("nbVertex"));
		    	list.add(m);
		    }
		}
		catch (Exception e) {
				    e.printStackTrace();System.out.println(e.getMessage());
		}
		finally
		{
			try{con.close();} catch (Exception e){return null;}
		}
		
		return list;
	}
	
	public static List<Modele> getListModeleByWord(String s){
		List<Modele> list = new ArrayList<Modele>();
		if(s==null){
			s="";
		}
		
		Connection con=null;
		try {
			Class.forName("org.sqlite.JDBC");
			con = DriverManager.getConnection("jdbc:sqlite:bdd/BddModele.db");
			Statement stmt = con.createStatement();
			Modele m=null;
			ResultSet rs = stmt.executeQuery("Select * from modeles where name like'%"+s+"%' or description like'%"+s+"%'");
			while(rs.next()){
				m = new Modele();
				m.setName(rs.getString("name"));
				m.setDescription(rs.getString("description"));
				m.setNbTriangles(rs.getInt("nbTriangle"));
				m.setNbVertex(rs.getInt("nbVertex"));
				list.add(m);
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally
		{
			try{con.close();} catch (Exception e){return null;}
		}
		
		return list;
	}
	
	public static int setTagForModele(String nameModele,String[] tag){
		ArrayList<String> list = new ArrayList<String>();
		for(int i=0;i<tag.length;i++){
			list.add(tag[i]);
		}
		return setTagForModele(nameModele,list);
	}

	public static int setTagForModele(String nameModele,List<String> tag){
		
		if(nameModele==null || tag==null ){
			return -1;
		}
		
		
		boolean succes=false;
		
		Connection con=null;
		try {
		    Class.forName("org.sqlite.JDBC");
		    con = DriverManager.getConnection("jdbc:sqlite:bdd/BddModele.db");
		    Statement stmt = con.createStatement();
		    
		    stmt.executeUpdate("PRAGMA foreign_keys = ON");
		    stmt.executeUpdate("Delete from tag where nameMod='"+nameModele+"'");
		    ResultSet rs = stmt.executeQuery("Select * from modeles where name ='"+nameModele+"'");
		    if(!rs.next()){
		    	System.out.println("Le modele n'existe pas");
		    	con.close();
		    	return -1;
		    }
		    for(String t : tag){
		    	stmt.executeUpdate("Insert into tag values('"+nameModele+"','"+t+"')");
		    }
		    succes=true;

		}
		catch (Exception e) {
				    e.printStackTrace();
		}
		finally
		{
			try{con.close();} catch (Exception e){return -1;}
		}
		
		if(succes){
			return 0;
		}
		return -1;
	}
	
	public static List<Modele> getListModeleSearchByTag(String s){
		List<Modele> list = new ArrayList<Modele>();
		if(s==null){
			s="";
		}
		
		Connection con=null;
		try {
			Class.forName("org.sqlite.JDBC");
			con = DriverManager.getConnection("jdbc:sqlite:bdd/BddModele.db");
			Statement stmt = con.createStatement();
			Modele m=null;
			ResultSet rs = stmt.executeQuery("Select * from modeles where name in (select nameMod from tag where tag like'"+s+"')");
			while(rs.next()){
				m = new Modele();
				m.setName(rs.getString("name"));
				m.setDescription(rs.getString("description"));
				m.setNbTriangles(rs.getInt("nbTriangle"));
				m.setNbVertex(rs.getInt("nbVertex"));
				list.add(m);
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally
		{
			try{con.close();} catch (Exception e){return null;}
		}
		
		return list;
	}
	
	private static void deleteModeleByName(String name){
		if(name==null){
			System.out.println("Il manque le nom");
		}

		Connection con=null;
		try {
		    Class.forName("org.sqlite.JDBC");
		    con = DriverManager.getConnection("jdbc:sqlite:bdd/BddModele.db");
		    Statement stmt = con.createStatement();
		    stmt.executeUpdate("PRAGMA foreign_keys = ON");
		    stmt.executeUpdate("delete from modeles where name='"+name+"'");
		    
		}
		catch (Exception e) {
				    e.printStackTrace();
		}
		finally
		{
			try{con.close();} catch (Exception e){}
		}
		
	}
	
	public static List<String> getListTagByModele(String nomModele){
		List<String> list = new ArrayList<String>();
		if(nomModele==null){
			return null;
		}
		boolean succes=false;
		
		Connection con=null;
		try {
		    Class.forName("org.sqlite.JDBC");
		    con = DriverManager.getConnection("jdbc:sqlite:bdd/BddModele.db");
		    Statement stmt = con.createStatement();
		    ResultSet rs = stmt.executeQuery("Select tag from tag where nameMod='"+nomModele+"'");
		    while(rs.next()){
		    	list.add(rs.getString("tag"));
		    }
		    succes=true;
		    
		}
		catch (Exception e) {
				    e.printStackTrace();System.out.println(e.getMessage());
		}
		finally
		{
			try{con.close();} catch (Exception e){return null;}
		}
		
		if(succes){
			return list;
		}else{
			return null;
		}
	}
	
	
	public static void main(String[] args) {
		String modele="pikachu";
		DataBaseInterface.deleteModeleByName(modele);
		//DataBaseInterface.addModele(modele, "models/"+modele+".gts", "ceci est "+modele+"");
		//DataBaseInterface.setDescription("head", "ceci est une trÃ¨s belle tete");
		//DataBaseInterface.setName("pikachu", "pikachu2");
		Modele3D m = null;
		m=DataBaseInterface.getModele(modele);
		
		
	
		
		if(m!=null){
			Object3D o=new Object3D(m);
			System.out.println(o.getWidth()+" "+o.getHeight()+" "+o.getDepth());
			System.out.println("CenterPoint : "+o.get2DCenterPoint());
			javax.swing.JFrame jf=new Display(new Renderer(o));
		}
	
	}
}