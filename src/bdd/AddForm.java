package bdd;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.filechooser.FileNameExtensionFilter;

import Renderer3d.Display;

public class AddForm extends JFrame{

	private static final long serialVersionUID = 1L;

	private Display mother;
	private JFileChooser fileChooser;
	private JTextField nom;
	private JTextField description;
	private JTextField tag;
	private JLabel filePath;
	private JButton boutonFileChooser;
	private JButton boutonEnvoi;
	
	public AddForm(Display mother){
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setVisible(true);
		setPreferredSize(new Dimension(300, 400));
		this.mother=mother;
		
		fileChooser=new JFileChooser();
		fileChooser.setFileFilter(new FileNameExtensionFilter("*.gts", "gts"));
		
		filePath=new JLabel();
		
		tag=new JTextField();
		
		
		GridLayout layout = new GridLayout(0, 1);
		this.setLayout(layout);
		
		this.add(new JLabel("Nom du modele"));
		
		nom=new JTextField();
		this.add(nom);
		
		boutonEnvoi=new JButton("Enregistrer le modele");
		boutonEnvoi.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				boutonEnvoiListener(e);
			}
		});
		
		boutonFileChooser=new JButton("Choisir un fichier gts");
		boutonFileChooser.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				boutonFileListener(arg0);
			}
		});
		this.add(boutonFileChooser);
		this.add(filePath);
		
		
		this.add(new JLabel("Description du modele"));
		
		description=new JTextField();
		this.add(description);
		this.add(new JLabel("Tags (separes par un espace)"));
		this.add(new JLabel("exemple : tag1 tag2 tag3"));
		this.add(tag);
		this.add(boutonEnvoi);
		pack();
	}
	
	private void boutonEnvoiListener(ActionEvent e){
		DataBaseInterface.addModele(nom.getText(), filePath.getText().substring(17), description.getText());
		DataBaseInterface.setTagForModele(nom.getText(), tag.getText().split(" "));
		mother.reloadMenuBar();
		JOptionPane.showMessageDialog(mother,"Le modele "+nom.getText()+" a ete correctement ajoute a la base de donnee !");
		this.dispose();
	}
	
	private void boutonFileListener(ActionEvent e){
		int retour=fileChooser.showOpenDialog(this);
		if(retour==JFileChooser.APPROVE_OPTION){
			
			
			String path = fileChooser.getSelectedFile().getAbsolutePath().replace('\\', '/');
		   filePath.setText("Fichier Choisie: "+path);
		}
	}
	
	public static void main(String[] args) {
		//System.out.println(o.getWidth()+" "+o.getHeight()+" "+o.getDepth());
		//System.out.println("CenterPoint : "+o.get3DCenterPoint());
		//new Display(new Renderer(o));
	
		
		new AddForm(null);
	}
}
