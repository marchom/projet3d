package bdd;

import Renderer3d.*;

public class Modele3D extends Modele{

	private Vertex vertices[];
	private Triangle triangles[];
	
	
	public Vertex[] getVertices() {
		return vertices;
	}

	public void setVertices(Vertex[] vertices) {
		this.vertices = vertices;
	}

	public Triangle[] getTriangles() {
		return triangles;
	}

	public void setTriangles(Triangle[] triangles) {
		this.triangles = triangles;
	}
}
