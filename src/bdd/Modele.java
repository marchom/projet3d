package bdd;

public class Modele {
	private String name;
	private String description;
	private int nbTriangles;
	private int nbVertex;

	public int getNbTriangles() {
		return nbTriangles;
	}


	public void setNbTriangles(int nbTriangles) {
		this.nbTriangles = nbTriangles;
	}

	public int getNbVertex() {
		return nbVertex;
	}

	public void setNbVertex(int nbVertex) {
		this.nbVertex = nbVertex;
	}

	public Modele(){
		
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
}
