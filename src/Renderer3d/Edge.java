package Renderer3d;
public class Edge {
	Vertex[] vertices= new Vertex[2];
	public Edge(Vertex a,Vertex b){
		vertices[0]=a;
		vertices[1]=b;
	}
	public String toString(){
		return new String(vertices[0].toString()+" "+vertices[1].toString());
	}
}
