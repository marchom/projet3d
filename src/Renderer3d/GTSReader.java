package Renderer3d;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class GTSReader {
	ArrayList<String> fileContent;
	Vertex[] vertices;
	public GTSReader(String filePath){
		fileContent=getContent(filePath);
		vertices=getVertices();
	}
	public ArrayList<String> getContent(String filePath){
		ArrayList<String> fileContent=new ArrayList<String>();
		File file = new File(filePath);
		try{
			Scanner scanner =  new Scanner(file);
			while (scanner.hasNextLine()){fileContent.add(scanner.nextLine());}
			scanner.close();
		}catch(IOException e){e.printStackTrace();}
		//for(String s:fileContent) System.out.println(s);
		return fileContent;
	}
	public int getNBVertices(){return Integer.parseInt(fileContent.get(0).split(" ")[0]);}
	public int getNBEdges(){return Integer.parseInt(fileContent.get(0).split(" ")[1]);}
	public int getNBTriangles(){return Integer.parseInt(fileContent.get(0).split(" ")[2]);}
	public Vertex getVertex(int i){
		//System.out.println("V : "+fileContent.get(i));
		return new Vertex(Double.parseDouble(fileContent.get(i).split(" ")[0]),
				Double.parseDouble(fileContent.get(i).split(" ")[1]),
				Double.parseDouble(fileContent.get(i).split(" ")[2]));
	}
	public int getVertexByEdge(int i,int n){
		//System.out.println("V : "+fileContent.get(i));
		return Integer.parseInt(fileContent.get(1+getNBVertices()+i).split(" ")[n])-1;
	}
	/**
	public Edge getEdge(int i){
		//System.out.println("E : "+fileContent.get(1+getNBVertices()+i));
		return new Edge(getVertex(Integer.parseInt(fileContent.get(1+getNBVertices()+i).split(" ")[0])),
				getVertex(Integer.parseInt(fileContent.get(1+getNBVertices()+i).split(" ")[1])));
	}**/
	public Triangle getTriangle(int i){
		//System.out.println("T : "+fileContent.get(1+getNBVertices()+getNBEdges()+i));
		int a=0,b=0,c=0;
		a=getVertexByEdge(Integer.parseInt(fileContent.get(1+getNBVertices()+getNBEdges()+i).split(" ")[0])-1,0);
		b=getVertexByEdge(Integer.parseInt(fileContent.get(1+getNBVertices()+getNBEdges()+i).split(" ")[1])-1,0);
		c=getVertexByEdge(Integer.parseInt(fileContent.get(1+getNBVertices()+getNBEdges()+i).split(" ")[2])-1,0);
		if(a==b||a==c||b==c){
			a=getVertexByEdge(Integer.parseInt(fileContent.get(1+getNBVertices()+getNBEdges()+i).split(" ")[0])-1,1);
			b=getVertexByEdge(Integer.parseInt(fileContent.get(1+getNBVertices()+getNBEdges()+i).split(" ")[1])-1,1);
			c=getVertexByEdge(Integer.parseInt(fileContent.get(1+getNBVertices()+getNBEdges()+i).split(" ")[2])-1,1);
		}
		if(a==b||a==c||b==c){
			a=getVertexByEdge(Integer.parseInt(fileContent.get(1+getNBVertices()+getNBEdges()+i).split(" ")[0])-1,1);
			b=getVertexByEdge(Integer.parseInt(fileContent.get(1+getNBVertices()+getNBEdges()+i).split(" ")[1])-1,1);
			c=getVertexByEdge(Integer.parseInt(fileContent.get(1+getNBVertices()+getNBEdges()+i).split(" ")[2])-1,0);
		}
		if(a==b||a==c||b==c){
			a=getVertexByEdge(Integer.parseInt(fileContent.get(1+getNBVertices()+getNBEdges()+i).split(" ")[0])-1,1);
			b=getVertexByEdge(Integer.parseInt(fileContent.get(1+getNBVertices()+getNBEdges()+i).split(" ")[1])-1,0);
			c=getVertexByEdge(Integer.parseInt(fileContent.get(1+getNBVertices()+getNBEdges()+i).split(" ")[2])-1,0);
		}
		if(a==b||a==c||b==c){
			a=getVertexByEdge(Integer.parseInt(fileContent.get(1+getNBVertices()+getNBEdges()+i).split(" ")[0])-1,0);
			b=getVertexByEdge(Integer.parseInt(fileContent.get(1+getNBVertices()+getNBEdges()+i).split(" ")[1])-1,1);
			c=getVertexByEdge(Integer.parseInt(fileContent.get(1+getNBVertices()+getNBEdges()+i).split(" ")[2])-1,1);
		}
		if(a==b||a==c||b==c){
			a=getVertexByEdge(Integer.parseInt(fileContent.get(1+getNBVertices()+getNBEdges()+i).split(" ")[0])-1,0);
			b=getVertexByEdge(Integer.parseInt(fileContent.get(1+getNBVertices()+getNBEdges()+i).split(" ")[1])-1,0);
			c=getVertexByEdge(Integer.parseInt(fileContent.get(1+getNBVertices()+getNBEdges()+i).split(" ")[2])-1,1);
		}
		if(a==b||a==c||b==c){
			a=getVertexByEdge(Integer.parseInt(fileContent.get(1+getNBVertices()+getNBEdges()+i).split(" ")[0])-1,1);
			b=getVertexByEdge(Integer.parseInt(fileContent.get(1+getNBVertices()+getNBEdges()+i).split(" ")[1])-1,0);
			c=getVertexByEdge(Integer.parseInt(fileContent.get(1+getNBVertices()+getNBEdges()+i).split(" ")[2])-1,1);
		}
		if(a==b||a==c||b==c){
			a=getVertexByEdge(Integer.parseInt(fileContent.get(1+getNBVertices()+getNBEdges()+i).split(" ")[0])-1,0);
			b=getVertexByEdge(Integer.parseInt(fileContent.get(1+getNBVertices()+getNBEdges()+i).split(" ")[1])-1,1);
			c=getVertexByEdge(Integer.parseInt(fileContent.get(1+getNBVertices()+getNBEdges()+i).split(" ")[2])-1,0);
		}
		return new Triangle(a,b,c);
	}
	public Vertex[] getVertices(){
		Vertex[] result=new Vertex[getNBVertices()];
		for(int i=0;i<result.length;i++){
			result[i]=getVertex(i+1);
		}
		return result;
	}
	/**
	public Edge[] getEdges(){
		Edge[] result=new Edge[getNBEdges()];
		for(int i=0;i<result.length;i++){
			result[i]=getEdge(i);
		}
		return result;
	}**/
	public Triangle[] getTriangles(){
		Triangle[] result=new Triangle[getNBTriangles()];
		for(int i=0;i<result.length;i++){
			result[i]=getTriangle(i);
		}
		//for(Triangle t:result) System.out.println(t);
		return result;
	}
	public boolean check(){
		//vérifie que les 3 caractéristiques du .gts existent
		if(fileContent.get(0).split(" ").length!=3) return false;
		//nombre de sommets
		int nbVertices=getNBVertices();
		//nombre de côtés
		int nbEdges= getNBEdges();
		//nombre de faces
		int nbTriangles=getNBTriangles();
		int i;
		//vérifie tout les sommets
		for(i=fileContent.size()-nbVertices-nbEdges-nbTriangles;i<fileContent.size()-nbEdges-nbTriangles;i++){
			//vérifie que le sommet est constitué de 3 valeurs
			if(fileContent.get(i).split(" ").length!=3) return false;
		}
		//vérifie tout les segments
		for(i=fileContent.size()-nbEdges-nbTriangles;i<fileContent.size()-nbTriangles;i++){
			//vérifie que le segment est constitué de 2 sommets
			if(fileContent.get(i).split(" ").length!=2||
				//vérifie que le 1er sommet est compris entre 0 et le nombre de sommets
				(Integer.parseInt(fileContent.get(i).split(" ")[0])>nbEdges||Integer.parseInt(fileContent.get(i).split(" ")[0])<0)||
				//vérifie que le 2eme sommet est compris entre 0 et le nombre de sommets
				(Integer.parseInt(fileContent.get(i).split(" ")[1])>nbEdges||Integer.parseInt(fileContent.get(i).split(" ")[1])<0)||
				//vérifie que les deux sommets ne soient pas les mêmes
				(Integer.parseInt(fileContent.get(i).split(" ")[0])==Integer.parseInt(fileContent.get(i).split(" ")[1]))){
				return false;
			}
		}
		//vérifie tout les triangles
		for(i=fileContent.size()-nbTriangles;i<fileContent.size();i++){
			//vérifie que le triangle est constitué de 3 segments
			if(fileContent.get(i).split(" ").length!=3||
				//vérifie que le 1er segment est compris entre 0 et le nombre de segments
				(Integer.parseInt(fileContent.get(i).split(" ")[0])>nbEdges||Integer.parseInt(fileContent.get(i).split(" ")[0])<0)||
				//vérifie que le 2eme segment est compris entre 0 et le nombre de segments
				(Integer.parseInt(fileContent.get(i).split(" ")[1])>nbEdges||Integer.parseInt(fileContent.get(i).split(" ")[1])<0)||
				//vérifie que le 3eme segment est compris entre 0 et le nombre de segments
				(Integer.parseInt(fileContent.get(i).split(" ")[2])>nbEdges||Integer.parseInt(fileContent.get(i).split(" ")[2])<0)||
				//vérifie que les deux premiers sommets ne soient pas les mêmes
				(Integer.parseInt(fileContent.get(i).split(" ")[0])==Integer.parseInt(fileContent.get(i).split(" ")[1]))||
				//vérifie que les deux derniers sommets ne soient pas les mêmes
				(Integer.parseInt(fileContent.get(i).split(" ")[1])==Integer.parseInt(fileContent.get(i).split(" ")[2]))||
				//vérifie que le premier et le deuxième sommets ne soient pas les mêmes
				(Integer.parseInt(fileContent.get(i).split(" ")[0])==Integer.parseInt(fileContent.get(i).split(" ")[2]))){
				return false;
			}
		}
		return true;
	}
}