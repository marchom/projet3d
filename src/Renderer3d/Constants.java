package Renderer3d;

import java.awt.Color;

public class Constants{
	//renderer stuff
	final static Color renderer_background_color=Color.BLACK;
	final static int renderer_axes_length=100;
	//console stuff
	final static float console_height=(float) 0.2;
	final static Color console_font_color=new Color(45,90,0);
	final static Color console_border_color_in=Color.GRAY;
	final static Color console_border_color_out=Color.WHITE;
	//rotation stuff
	final static double oneDegree=(Math.PI/(double)180.0);
	final static double[][] xRotationMatrix={{         1.0         ,         0.0         ,         0.0         },
											 {         0.0         , Math.cos(oneDegree) ,-Math.sin(oneDegree) },
											 {         0.0         , Math.sin(oneDegree) , Math.cos(oneDegree) }};
	
	final static double[][] yRotationMatrix={{ Math.cos(oneDegree) ,         0.0         , Math.sin(oneDegree) },
											 {         0.0         ,         1.0         ,         0.0         },
											 {-Math.sin(oneDegree) ,         0.0         , Math.cos(oneDegree) }};
	
	final static double[][] zRotationMatrix={{ Math.cos(oneDegree) ,-Math.sin(oneDegree) ,         0.0         },
											 { Math.sin(oneDegree) , Math.cos(oneDegree) ,         0.0         },
											 {         0.0         ,         0.0         ,         1.0         }};
}
