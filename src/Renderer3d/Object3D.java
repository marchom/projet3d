package Renderer3d;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.util.Arrays;
import java.util.Comparator;

import javax.swing.JOptionPane;

public class Object3D{
	int nbVertices,nbTriangles;
	public Vertex selectedVertex1=null;
	public Vertex selectedVertex2=null;
	Vertex light = new Vertex(1,0,0);
	Vertex[] vertices;
	Triangle[] triangles;
	BufferedImage image;
	String name="";
	Double[] volume=null;
	public double zoom=10;
	int xTranslate=300;
	int yTranslate=300;
	int eye=1000;
	//copie o3d vers cet objet
	public Object3D(Object3D o3d){
		vertices=new Vertex[o3d.nbVertices];
		for(int i=0;i<o3d.nbVertices;i++){
			vertices[i]=new Vertex(o3d.vertices[i].x,o3d.vertices[i].y,o3d.vertices[i].z);
		}
		triangles=new Triangle[o3d.nbTriangles];
		for(int i=0;i<o3d.nbTriangles;i++){
			triangles[i]=new Triangle(o3d.triangles[i].idxVertices[0],
								      o3d.triangles[i].idxVertices[1],
									  o3d.triangles[i].idxVertices[2]);
		}
		setVoisins();
	}	
	public Object3D(String filePath,String name){
		this(filePath);
		this.name=name;
	}
	public Object3D(String filePath){
		GTSReader gtsr=new GTSReader(filePath);
		if(!gtsr.check())JOptionPane.showMessageDialog(null,"Fichier GTS corrompu !");
		else{
			nbVertices=gtsr.getNBVertices();
			nbTriangles=gtsr.getNBTriangles();
			vertices=gtsr.getVertices();
			triangles=gtsr.getTriangles();
			sortTrianglesByZ();
		}
		setVoisins();
	}
	public Object3D(bdd.Modele3D m){
		vertices=m.getVertices();
		triangles=m.getTriangles();
		nbVertices=vertices.length;
		nbTriangles=triangles.length;
	}
	public void sortTrianglesByZ(){
		Arrays.sort(triangles, new Comparator<Triangle>() {
		    @Override
		    public int compare(Triangle o1, Triangle o2) {
		        return o2.compareTo(o1,vertices);
		    }
		});
	}
	public void setTrianglesShade(boolean lightLayout){
		double nNorme;
		double vNorme;
		double vProduct;
		double theta;
		int scale = (int)(nbTriangles/125);
		int plus = 0;
		int sum = 0;
		int nbVoisins = 1;
		for(int z=0;z<nbTriangles;z++){
			triangles[z].setVector(vertices);
			nNorme = Math.sqrt((triangles[z].normal.x)*(triangles[z].normal.x) + (triangles[z].normal.y)*(triangles[z].normal.y) + (triangles[z].normal.z)*(triangles[z].normal.z));
			vNorme = Math.sqrt((light.x)*(light.x) + (light.y)*(light.y) + (light.z)*(light.z));
			vProduct = triangles[z].normal.x*light.x + triangles[z].normal.y*light.y +triangles[z].normal.z*light.z;
			theta = vProduct/(nNorme*vNorme);
			if(z%scale==0){
				plus++;
			}
			triangles[z].shade = (int)((Math.cos(theta)*125))+plus;
		}
		if(lightLayout){
			for(int z=0;z<nbTriangles;z++){
				for(Integer idx : triangles[z].voisins){
					if(!triangles[idx].colored){
						sum += triangles[idx].shade;
						nbVoisins++;
					}
	
					
				}
				sum += triangles[z].shade;
				triangles[z].shade = (int)(sum/nbVoisins);
				triangles[z].colored = true;
				sum = 0;
				nbVoisins = 1;
			}
			for(int z=0;z<nbTriangles;z++){
				triangles[z].colored = false;
			}
		}
	}
	
	public void setVoisins(){
		for(int z=0;z<nbTriangles;z++){
			for(int y=0;y<nbTriangles;y++){
				if(z!=y && ((triangles[z].idxVertices[0]==triangles[y].idxVertices[0])||
					(triangles[z].idxVertices[0]==triangles[y].idxVertices[1] )||
					(triangles[z].idxVertices[0]==triangles[y].idxVertices[2] )||
					(triangles[z].idxVertices[1]==triangles[y].idxVertices[0] )||
					(triangles[z].idxVertices[1]==triangles[y].idxVertices[1] )||
					(triangles[z].idxVertices[1]==triangles[y].idxVertices[2] )||
					(triangles[z].idxVertices[2]==triangles[y].idxVertices[0] )||
					(triangles[z].idxVertices[2]==triangles[y].idxVertices[1] )||
					(triangles[z].idxVertices[2]==triangles[y].idxVertices[2] ))){
						triangles[z].voisins.add(y);
				}
			}
		}
	}
	public void rotateAroundX(double angle){
		if(angle>360) angle=360.0;
		if(angle<-360) angle=-360.0;
		//(Math.PI/(double)180.0 équivaut à 1°
		angle=angle*Constants.oneDegree;
		double[][] vertices=new double[nbVertices][3];
		for(int i=0;i<vertices.length;i++){
			vertices[i][0]=this.vertices[i].x;
			vertices[i][1]=this.vertices[i].y;
			vertices[i][2]=this.vertices[i].z;
		}
		double[][] rotation={{1.0,     0.0       ,       0.0      },
							 {0.0,Math.cos(angle),-Math.sin(angle)},
							 {0.0,Math.sin(angle),Math.cos(angle)}};
		vertices=Matrix.getTransposed(Matrix.multiply(rotation,Matrix.getTransposed(vertices)));
		//axes=Matrix.getTransposed(Matrix.multiply(rotation,Matrix.getTransposed(axes)));
		for(int i=0;i<vertices.length;i++){
			this.vertices[i].x=vertices[i][0];
			this.vertices[i].y=vertices[i][1];
			this.vertices[i].z=vertices[i][2];
		}
		sortTrianglesByZ();
	}
	public void rotateAroundY(double angle){
		if(angle>360) angle=360.0;
		if(angle<-360) angle=-360.0;
		//(Math.PI/(double)180.0 équivaut à 1°
		angle=angle*Constants.oneDegree;
		double[][] vertices=new double[nbVertices][3];
		for(int i=0;i<vertices.length;i++){
			vertices[i][0]=this.vertices[i].x;
			vertices[i][1]=this.vertices[i].y;
			vertices[i][2]=this.vertices[i].z;
		}
		double[][] rotation={{ Math.cos(angle),0.0,Math.sin(angle)},
							 {       0.0      ,1.0,      0.0      },
							 {-Math.sin(angle),0.0,Math.cos(angle)}};
		vertices=Matrix.getTransposed(Matrix.multiply(rotation,Matrix.getTransposed(vertices)));
		for(int i=0;i<vertices.length;i++){
			this.vertices[i].x=vertices[i][0];
			this.vertices[i].y=vertices[i][1];
			this.vertices[i].z=vertices[i][2];
		}
		sortTrianglesByZ();
	}
	public void rotateAroundZ(double angle){
		if(angle>360) angle=360.0;
		if(angle<-360) angle=-360.0;
		//(Math.PI/(double)180.0 équivaut à 1°
		angle=angle*Constants.oneDegree;
		double[][] vertices=new double[nbVertices][3];
		for(int i=0;i<vertices.length;i++){
			vertices[i][0]=this.vertices[i].x;
			vertices[i][1]=this.vertices[i].y;
			vertices[i][2]=this.vertices[i].z;
		}
		double[][] rotation={{Math.cos(angle),-Math.sin(angle),0.0},
							 {Math.sin(angle), Math.cos(angle),0.0},
							 {      0.0      ,      0.0       ,1.0}};
		vertices=Matrix.getTransposed(Matrix.multiply(rotation,Matrix.getTransposed(vertices)));
		for(int i=0;i<vertices.length;i++){
			this.vertices[i].x=vertices[i][0];
			this.vertices[i].y=vertices[i][1];
			this.vertices[i].z=vertices[i][2];
		}
		sortTrianglesByZ();
	}
	public void thouShaltScale(double x,double y,double z){
		double[][] vertices=new double[nbVertices][4];
		for(int i=0;i<vertices.length;i++){
			vertices[i][0]=this.vertices[i].x;
			vertices[i][1]=this.vertices[i].y;
			vertices[i][2]=this.vertices[i].z;
			vertices[i][3]=1;
		}
		double[][] translation={{  x  , 0.0 , 0.0 , 0.0 },
							 	{ 0.0 ,  y  , 0.0 , 0.0 },
							 	{ 0.0 , 0.0 ,  z  , 0.0 },
							 	{ 0.0 , 0.0 , 0.0 , 1.0 }};
		vertices=Matrix.getTransposed(Matrix.multiply(translation,Matrix.getTransposed(vertices)));
		for(int i=0;i<vertices.length;i++){
			this.vertices[i].x=vertices[i][0];
			this.vertices[i].y=vertices[i][1];
			this.vertices[i].z=vertices[i][2];
		}
	}
	public void thouShaltTranslate(double xTranslation,double yTranslation,double zTranslation){
		double[][] vertices=new double[nbVertices][4];
		for(int i=0;i<vertices.length;i++){
			vertices[i][0]=this.vertices[i].x;
			vertices[i][1]=this.vertices[i].y;
			vertices[i][2]=this.vertices[i].z;
			vertices[i][3]=1;
		}
		double[][] translation={{1.0 , 0.0 , 0.0 , xTranslation},
							 	{0.0 , 1.0 , 0.0 , yTranslation},
							 	{0.0 , 0.0 , 1.0 , zTranslation},
							 	{0.0 , 0.0 , 0.0 ,     1.0     }};
		vertices=Matrix.getTransposed(Matrix.multiply(translation,Matrix.getTransposed(vertices)));
		for(int i=0;i<vertices.length;i++){
			this.vertices[i].x=vertices[i][0];
			this.vertices[i].y=vertices[i][1];
			this.vertices[i].z=vertices[i][2];
		}
	}
	public void thouShaltFlattenByX(){
		double[][] vertices=new double[nbVertices][3];
		for(int i=0;i<vertices.length;i++){
			vertices[i][0]=this.vertices[i].x;
			vertices[i][1]=this.vertices[i].y;
			vertices[i][2]=this.vertices[i].z;
		}
		double[][] beFlatByX={{0.0 , 0.0 , 0.0 },
							  {0.0 , 1.0 , 0.0 },
							  {0.0 , 0.0 , 1.0 }};
		vertices=Matrix.getTransposed(Matrix.multiply(beFlatByX,Matrix.getTransposed(vertices)));
		for(int i=0;i<vertices.length;i++){
			this.vertices[i].x=vertices[i][0];
			this.vertices[i].y=vertices[i][1];
			this.vertices[i].z=vertices[i][2];
		}
	}
	public void thouShaltFlattenByY(){
		double[][] vertices=new double[nbVertices][3];
		for(int i=0;i<vertices.length;i++){
			vertices[i][0]=this.vertices[i].x;
			vertices[i][1]=this.vertices[i].y;
			vertices[i][2]=this.vertices[i].z;
		}
		double[][] beFlatByY={{1.0 , 0.0 , 0.0 },
							  {0.0 , 0.0 , 0.0 },
							  {0.0 , 0.0 , 1.0 }};
		vertices=Matrix.getTransposed(Matrix.multiply(beFlatByY,Matrix.getTransposed(vertices)));
		for(int i=0;i<vertices.length;i++){
			this.vertices[i].x=vertices[i][0];
			this.vertices[i].y=vertices[i][1];
			this.vertices[i].z=vertices[i][2];
		}
	}
	public void thouShaltFlattenByZ(){
		double[][] vertices=new double[nbVertices][3];
		for(int i=0;i<vertices.length;i++){
			vertices[i][0]=this.vertices[i].x;
			vertices[i][1]=this.vertices[i].y;
			vertices[i][2]=this.vertices[i].z;
		}
		double[][] beFlatByZ={{1.0 , 0.0 , 0.0 },
							  {0.0 , 1.0 , 0.0 },
							  {0.0 , 0.0 , 0.0 }};
		vertices=Matrix.getTransposed(Matrix.multiply(beFlatByZ,Matrix.getTransposed(vertices)));
		for(int i=0;i<vertices.length;i++){
			this.vertices[i].x=vertices[i][0];
			this.vertices[i].y=vertices[i][1];
			this.vertices[i].z=vertices[i][2];
		}
	}
	public double getWidth(){
		double minX=vertices[0].x;
		double maxX=vertices[0].x;
		for(Vertex v:vertices){
			if(v.x<minX) minX=v.x;
			if(v.x>maxX) maxX=v.x;
		}
		return ((minX<0&&maxX<0)?Math.abs(maxX-minX):(maxX-minX));
	}
	public double getHeight(){
		double minY=vertices[0].y;
		double maxY=vertices[0].y;
		for(Vertex v:vertices){
			if(v.y<minY) minY=v.y;
			if(v.y>maxY) maxY=v.y;
		}
		return ((minY<0&&maxY<0)?Math.abs(maxY-minY):(maxY-minY));
	}
	public double getDepth(){
		double minZ=vertices[0].z;
		double maxZ=vertices[0].z;
		for(Vertex v:vertices){
			if(v.z<minZ) minZ=v.z;
			if(v.z>maxZ) maxZ=v.z;
		}
		return ((minZ<0&&maxZ<0)?Math.abs(maxZ-minZ):(maxZ-minZ));
	}
	public double[] getRawVolume(){
		double minX=vertices[0].x;double maxX=vertices[0].x;
		double minY=vertices[0].y;double maxY=vertices[0].y;
		double minZ=vertices[0].z;double maxZ=vertices[0].z;
		for(Vertex v:vertices){
			if(v.x<minX) minX=v.x;
			if(v.x>maxX) maxX=v.x;
			if(v.y<minY) minY=v.y;
			if(v.y>maxY) maxY=v.y;
			if(v.z<minZ) minZ=v.z;
			if(v.z>maxZ) maxZ=v.z;
		}
		if(volume==null){
			volume=new Double[3];
			volume[0]=getWidth();volume[1]=getHeight();volume[2]=getDepth();
		}else if(volume[0]*volume[1]*volume[2]>getWidth()*getHeight()*getDepth()){
			volume[0]=getWidth();volume[1]=getHeight();volume[2]=getDepth();
		}
		return new double[]{getWidth(),getHeight(),getDepth()};
	}
	public double[] getVolume(){
		return new double[]{volume[0],volume[1],volume[2]};
	}
	public Vertex get3DCenterPoint(){
		return new Vertex(getWidth()/2,getHeight()/2,getDepth()/2);
	}
	public Point get2DCenterPoint(){
		return new Point((int)(getWidth()/2),(int)(getHeight()/2));
	}
	public void selectVertex(Point p,boolean perspective){
		if(selectedVertex1!=null&&selectedVertex2!=null){
			selectedVertex1=null;
			selectedVertex2=null;
		}else{
			Vertex closestVertex=null;
			for(Vertex v:vertices){
				if(p.distance(projectOnScreen(v,perspective))<10*zoom){
					//close enough
					//System.out.println("closenough:"+v);
					if(closestVertex==null)closestVertex=v;
					if(p.distance(projectOnScreen(v,perspective))<p.distance(projectOnScreen(closestVertex,perspective))){
						closestVertex=v;
					}
				}
			}
			if(selectedVertex1==null){
				selectedVertex1=closestVertex;
			}else if(selectedVertex2==null){
				selectedVertex2=closestVertex;
			}
		}
	}
	public Point projectOnScreen(Vertex v,boolean perspective){
		Vertex tmp=new Vertex(v.x,v.y,v.z);
		tmp.x=tmp.x*zoom+xTranslate;
		tmp.y=tmp.y*zoom+yTranslate;
		if(perspective){
			tmp.x=(-eye*(tmp.x-getWidth()/2))/(-eye+tmp.z)+getWidth()/2;
			tmp.y=(-eye*(tmp.y-getHeight()/2))/(-eye+tmp.z)+getHeight()/2;
		}
		return new Point((int)tmp.x,(int)tmp.y);
	}
	public double[] getYs(boolean perspective){
		double[] y=new double[3];
		for(Triangle t:triangles){
			for(int i=0;i<3;i++){
				y[i]=vertices[t.idxVertices[i]].y*zoom+yTranslate;
				if(perspective){
					y[i]=(-eye*(y[i]-getHeight()/2))/(-eye+vertices[t.idxVertices[i]].z*zoom)+getHeight()/2;
				}
			}
		}
		return y;
	}
	public double[] getXs(boolean perspective){
		double[] x=new double[3];
		for(Triangle t:triangles){
			for(int i=0;i<3;i++){
				x[i]=vertices[t.idxVertices[i]].x*zoom+yTranslate;
				if(perspective){
					x[i]=(-eye*(x[i]-getWidth()/2))/(-eye+vertices[t.idxVertices[i]].z*zoom)+getWidth()/2;
				}
			}
		}
		return x;
	}
	public Vertex[] getVertices(){return vertices;}
	public Triangle[] getTriangles(){return triangles;}
	public String toString(){
		String result="";
		for(Vertex v:vertices) result+=v.toString()+"\n";
		for(Triangle t:triangles) result+=t.toString()+"\n";
		return result;
	}
}