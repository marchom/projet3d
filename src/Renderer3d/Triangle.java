package Renderer3d;

import java.util.ArrayList;

public class Triangle{
	public Vertex normal;
	int[] idxVertices=new int[3];
	int shade;
	public ArrayList<Integer> voisins;
	boolean colored;
	public Triangle(int a,int b,int c){
		idxVertices[0]=a;
		idxVertices[1]=b;
		idxVertices[2]=c;
		shade=1;
		voisins = new ArrayList<Integer>();
		colored = false;
	}
	public int compareTo(Triangle t,Vertex[] vertices) {
		double sumZThisFace=vertices[this.idxVertices[0]].z+vertices[this.idxVertices[1]].z+vertices[this.idxVertices[2]].z;
		double sumZOtherFace=vertices[t.idxVertices[0]].z+vertices[t.idxVertices[1]].z+vertices[t.idxVertices[2]].z;
		if(sumZThisFace > sumZOtherFace){
			return 1;
		}else if(sumZThisFace < sumZOtherFace){
			return -1;
		}
		return 0;
	}
	public int[] getIdxVertices() {
		return idxVertices;
	}
	public void setVector(Vertex[] vertices){
		Vertex a = vertices[idxVertices[0]];
		Vertex b = vertices[idxVertices[1]];
		Vertex c = vertices[idxVertices[2]];
		normal = new Vertex((b.y-a.y)*(c.z-a.z)-(b.z-a.z)*(c.y-a.y),(b.y-a.y)*(c.x-a.x)-(b.x-a.x)*(c.z-a.z),(b.x-a.x)*(c.y-a.y)-(b.y-a.y)*(c.x-a.x));
	}
	public String toString(Vertex[] vertices){
		return new String(vertices[this.idxVertices[0]].toString()+
				" "+vertices[this.idxVertices[1]].toString()+
				" "+vertices[this.idxVertices[2]].toString());
	}
	

	
}
