package Renderer3d;
public class Vertex{
	double x,y,z;
	boolean direction;
	public Vertex(double x,double y,double z){
		this.x=x;
		this.y=y;
		this.z=z;
	}
	public String toString(){
		return new String(x+" "+y+" "+z);
	}
	public double getX() {
		return x;
	}
	public double getY() {
		return y;
	}
	public double getZ() {
		return z;
	}
	public double[] toArray(){
		return new double[]{x,y,z};
	}
}