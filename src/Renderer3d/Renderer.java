package Renderer3d;

import java.util.Random;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.geom.Path2D;
import java.awt.geom.Point2D;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import bdd.*;

public class Renderer extends JPanel implements MouseListener,MouseWheelListener,MouseMotionListener{

	private static final long serialVersionUID = 1L;
	public Object3D object;
	public Object3D initialObject;
	Point2D origin=new Point();
	Integer selectedSegmentSize=null;
	double[][] axes=new double[3][3];
	long lastPressed=0;
	float intensity=1/10;
	int eye=500;
	public boolean antiAliasing=false;
	public boolean perspective=true;
	public boolean information=true;
	Point pressed=null;
	private Image offscreenImage;
	private Graphics offscreenGraphics;
	private Dimension offscreenDimension;
	public boolean lightLayout=false;
	public boolean contours=false;
	public boolean randomColor=false;
	public Color randColor;
	public Random rand;
	
	public Renderer(Object3D o3d){
		rand = new Random();
		randColor=new Color(100,100,100);
		object=o3d;
		initialObject=new Object3D(o3d);
		this.setDoubleBuffered(true);
		addMouseListener(this);
		addMouseWheelListener(this);
		addMouseMotionListener(this);
	}
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Dimension currentSize = getSize();
		if(offscreenImage==null||!currentSize.equals(offscreenDimension)) {
			if(offscreenImage==null)centerObject();
			offscreenImage = createImage(currentSize.width, currentSize.height);
			offscreenGraphics = offscreenImage.getGraphics();
			offscreenDimension = currentSize;   
			//initAxes();
		}
		Graphics2D g2d=(Graphics2D)offscreenGraphics;
		//antialiasing = fpsdrop
		if(antiAliasing){
			RenderingHints rh = new RenderingHints(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
        	rh.put(RenderingHints.KEY_RENDERING,RenderingHints.VALUE_RENDER_QUALITY);
        	g2d.setRenderingHints(rh);
		}else{
			RenderingHints rh = new RenderingHints(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_OFF);
			g2d.setRenderingHints(rh);
		}
        g2d.setColor(Constants.renderer_background_color);
        g2d.fill(getBounds());
		drawObjects(g2d);
		drawInfo(g2d);
		g.drawImage(offscreenImage, 0, 0, this);
		/**if(object.image==null){
		    BufferedImage image = new BufferedImage(
		    	      getWidth(),
		    	      getHeight(),
		    	      BufferedImage.TYPE_INT_RGB
		    	      );
		    paint(image.getGraphics());
		    object.image=image;
		}**/
	}
	public void drawObjects(Graphics2D g2d) {
		double[] x=new double[3],y=new double[3];
		Path2D path=null;
		Triangle t=null;
		object.setTrianglesShade(lightLayout);
		for(int z=0;z<object.nbTriangles;z++){
			t = object.triangles[z];
			if(t.shade==0){
				t.shade=0;
			}
			if(t.shade>255){
				t.shade=255;
			}
			for(int i=0;i<3;i++){
				Vertex v=object.vertices[t.idxVertices[i]];
				x[i]=v.x*object.zoom+object.xTranslate;
				y[i]=v.y*object.zoom+object.yTranslate;
				//perspective : eye=distance des yeux par rapport a l'ecran
				if(perspective){
					x[i]=(-eye*(x[i]-getWidth()/2))/(-eye+v.z)+getWidth()/2;
					y[i]=(-eye*(y[i]-getHeight()/2))/(-eye+v.z)+getHeight()/2;
				}
			}
			path = new Path2D.Double();
			path.moveTo(x[0], y[0]);
			path.lineTo(x[1], y[1]);
			path.lineTo(x[2], y[2]);
			path.closePath();
			if(!randomColor){
				g2d.setColor(new Color(t.shade,t.shade,t.shade));
			}
			else{
				g2d.setColor(new Color((int)(Math.sqrt((randColor.getRed()-t.shade)*(randColor.getRed()-t.shade))),(int)(Math.sqrt((randColor.getGreen()-t.shade)*(randColor.getGreen()-t.shade))),(int)(Math.sqrt((randColor.getBlue()-t.shade)*(randColor.getBlue()-t.shade)))));
			}
			g2d.fill(path);
			if(contours){
				g2d.setColor(Color.BLACK);
			}
			g2d.draw(path);
		}
		if(object.selectedVertex1!=null){
			g2d.setColor(Color.RED);
			double tmpX=object.selectedVertex1.x*object.zoom+object.xTranslate;
			double tmpY=object.selectedVertex1.y*object.zoom+object.yTranslate;
			//perspective : eye=distance des yeux par rapport � l'�cran
			if(perspective){						
				tmpX=(-eye*(tmpX-getWidth()/2))/(-eye+object.selectedVertex1.z)+getWidth()/2;
				tmpY=(-eye*(tmpY-getHeight()/2))/(-eye+object.selectedVertex1.z)+getHeight()/2;
			}
			g2d.fillRect((int)tmpX-1,(int)tmpY-1,2,2);
			if(object.selectedVertex2!=null){
				Point p1=new Point((int)tmpX,(int)tmpY);
				tmpX=object.selectedVertex2.x*object.zoom+object.xTranslate;
				tmpY=object.selectedVertex2.y*object.zoom+object.yTranslate;
				//perspective : eye=distance des yeux par rapport � l'�cran
				if(perspective){
					tmpX=(-eye*(tmpX-getWidth()/2))/(-eye+object.selectedVertex1.z)+getWidth()/2;
					tmpY=(-eye*(tmpY-getHeight()/2))/(-eye+object.selectedVertex1.z)+getHeight()/2;
				}
				g2d.fillRect((int)tmpX-1,(int)tmpY-1,2,2);
				g2d.drawLine(p1.x,p1.y,(int)tmpX,(int)tmpY);
			}
		}
		/**
		for(int i=0;i<3;i++){
			path = new Path2D.Double();
			path.moveTo(origin.getX(),origin.getY());
			g2d.setColor(i==0?Color.RED:(i==1?Color.BLUE:Color.GREEN));
			path.lineTo(axes[i][0]*Constants.renderer_axes_length+origin.getX(),axes[i][1]*Constants.renderer_axes_length+origin.getY());
			path.closePath();
			g2d.draw(path);
		}
		**/
	}
	public void drawInfo(Graphics2D g2d){
		g2d.setColor(Constants.console_font_color);
		int placement=1;
		g2d.drawString("Nom de l'objet : "+object.name,
				getWidth()-getWidth()/4,placement*g2d.getFontMetrics().getHeight());
		placement++;
		g2d.drawString("Volume brut de l'objet : ",
				getWidth()-getWidth()/4,placement*g2d.getFontMetrics().getHeight());
		placement++;
		double[] vol=object.getRawVolume();
		g2d.drawString("   Volume brut virtuel : "+
				(double)Math.round(vol[0]*vol[1]*vol[2]*100000)/100000,
				getWidth()-getWidth()/4,placement*g2d.getFontMetrics().getHeight());
		placement++;
		Double tmp = 0.0;
		if(object.selectedVertex1!=null&&object.selectedVertex2!=null&&selectedSegmentSize!=null){
			double distance =Math.sqrt((object.selectedVertex1.x-object.selectedVertex2.x)*(object.selectedVertex1.x-object.selectedVertex2.x)+
					(object.selectedVertex1.y-object.selectedVertex2.y)*(object.selectedVertex1.y-object.selectedVertex2.y)+
					(object.selectedVertex1.z-object.selectedVertex2.z)*(object.selectedVertex1.z-object.selectedVertex2.z));
			tmp=vol[0]*(selectedSegmentSize/distance)*
					vol[1]*(selectedSegmentSize/distance)*
					vol[2]*(selectedSegmentSize/distance);
		}
		g2d.drawString("   Volume brut reel : "+
				((object.selectedVertex2==null&&selectedSegmentSize==null)?"Pas de segment selectionne":
						((double)Math.round(tmp*100000)/100000)+"cm cube"),
				getWidth()-getWidth()/4,placement*g2d.getFontMetrics().getHeight());
		placement++;
		vol=object.getVolume();
		g2d.drawString("Volume net de l'objet : ",
				getWidth()-getWidth()/4,placement*g2d.getFontMetrics().getHeight());
		placement++;
		g2d.drawString("   Volume net virtuel : "+
				(double)Math.round(vol[0]*vol[1]*vol[2]*100000)/100000,
				getWidth()-getWidth()/4,placement*g2d.getFontMetrics().getHeight());
		placement++;
		tmp = 0.0;
		if(object.selectedVertex1!=null&&object.selectedVertex2!=null&&selectedSegmentSize!=null){
			double distance =Math.sqrt((object.selectedVertex1.x-object.selectedVertex2.x)*(object.selectedVertex1.x-object.selectedVertex2.x)+
					(object.selectedVertex1.y-object.selectedVertex2.y)*(object.selectedVertex1.y-object.selectedVertex2.y)+
					(object.selectedVertex1.z-object.selectedVertex2.z)*(object.selectedVertex1.z-object.selectedVertex2.z));
			tmp=vol[0]*(selectedSegmentSize/distance)*
					vol[1]*(selectedSegmentSize/distance)*
					vol[2]*(selectedSegmentSize/distance);
		}
		g2d.drawString("   Volume net reel : "+
				((object.selectedVertex2==null&&selectedSegmentSize==null)?"Pas de segment selectionne":
						((double)Math.round(tmp*100000)/100000)+"cm cube"),
				getWidth()-getWidth()/4,placement*g2d.getFontMetrics().getHeight());
		placement++;
		g2d.drawString("Nombre de triangles : "+object.triangles.length,
				getWidth()-getWidth()/4,placement*g2d.getFontMetrics().getHeight());
		placement++;
		g2d.drawString("Zoom : "+object.zoom,
				getWidth()-getWidth()/4,placement*g2d.getFontMetrics().getHeight());
		placement++;
		g2d.drawString("Distortion : "+eye,
				getWidth()-getWidth()/4,placement*g2d.getFontMetrics().getHeight());
		placement++;
		g2d.drawString("Anti-Aliasing : "+(antiAliasing?"Actif":"Inactif"),
				getWidth()-getWidth()/4,placement*g2d.getFontMetrics().getHeight());
		placement++;
		g2d.drawString("Perspective : "+(perspective?"Actif":"Inactif"),
				getWidth()-getWidth()/4,placement*g2d.getFontMetrics().getHeight());
		placement++;
		g2d.drawString("Lissage lumiere : "+(lightLayout?"Actif":"Inactif"),
				getWidth()-getWidth()/4,placement*g2d.getFontMetrics().getHeight());
		placement++;
		g2d.drawString("Contours : "+(contours?"Actif":"Inactif"),
				getWidth()-getWidth()/4,placement*g2d.getFontMetrics().getHeight());
		placement++;
		g2d.drawString("Segment selectionne : ",
				getWidth()-getWidth()/4,placement*g2d.getFontMetrics().getHeight());
		placement++;
		g2d.drawString("   Premier sommet : ",
				getWidth()-getWidth()/4,placement*g2d.getFontMetrics().getHeight());
		placement++;
		if(object.selectedVertex1!=null){
			g2d.drawString("      X : "+(double)Math.round(object.selectedVertex1.x*1000)/1000,
					getWidth()-getWidth()/4,placement*g2d.getFontMetrics().getHeight());
			placement++;
			g2d.drawString("      Y : "+(double)Math.round(object.selectedVertex1.y*1000)/1000,
					getWidth()-getWidth()/4,placement*g2d.getFontMetrics().getHeight());
			placement++;
			g2d.drawString("      Z : "+(double)Math.round(object.selectedVertex1.z*1000)/1000,
					getWidth()-getWidth()/4,placement*g2d.getFontMetrics().getHeight());
			placement++;
		}else{
			g2d.drawString("      Aucun(Shift + clic gauche pour selectionne un sommet)",
					getWidth()-getWidth()/4,placement*g2d.getFontMetrics().getHeight());
			placement++;
		}
		g2d.drawString("   Second sommet : ",
				getWidth()-getWidth()/4,placement*g2d.getFontMetrics().getHeight());
		placement++;
		if(object.selectedVertex2!=null){
			g2d.drawString("      X : "+(double)Math.round(object.selectedVertex2.x*1000)/1000,
					getWidth()-getWidth()/4,placement*g2d.getFontMetrics().getHeight());
			placement++;
			g2d.drawString("      Y : "+(double)Math.round(object.selectedVertex2.y*1000)/1000,
					getWidth()-getWidth()/4,placement*g2d.getFontMetrics().getHeight());
			placement++;
			g2d.drawString("      Z : "+(double)Math.round(object.selectedVertex2.z*1000)/1000,
					getWidth()-getWidth()/4,placement*g2d.getFontMetrics().getHeight());
			placement++;
		}else{
			g2d.drawString("      Aucun(Shift + clic gauche pour selectionne un sommet)",
					getWidth()-getWidth()/4,placement*g2d.getFontMetrics().getHeight());
			placement++;
		}
		g2d.drawString("Taille du segment selectionne : ",
				getWidth()-getWidth()/4,placement*g2d.getFontMetrics().getHeight());
		placement++;
		g2d.drawString("   Taille reelle : "+((selectedSegmentSize==null)?0:selectedSegmentSize)+"cm",
				getWidth()-getWidth()/4,placement*g2d.getFontMetrics().getHeight());
		placement++;
		g2d.drawString("   Taille virtuelle : "+((selectedSegmentSize==null||object.selectedVertex1==null||object.selectedVertex2==null)?0:
				(double)Math.round(Math.sqrt((object.selectedVertex1.x-object.selectedVertex2.x)*(object.selectedVertex1.x-object.selectedVertex2.x)+
						(object.selectedVertex1.y-object.selectedVertex2.y)*(object.selectedVertex1.y-object.selectedVertex2.y)+
						(object.selectedVertex1.z-object.selectedVertex2.z)*(object.selectedVertex1.z-object.selectedVertex2.z))*100)/100),
				getWidth()-getWidth()/4,placement*g2d.getFontMetrics().getHeight());
		placement++;
	}
	public void initAxes(){
		if(origin.getX()==0&&origin.getY()==0){
			axes[0][0]=1;axes[0][1]=0;axes[0][2]=0;
			axes[1][0]=0;axes[1][1]=-1;axes[1][2]=0;
			axes[2][0]=0;axes[2][1]=0;axes[2][2]=1;
		}
		origin.setLocation(getWidth()/20+Constants.renderer_axes_length,getHeight()-getHeight()/20-Constants.renderer_axes_length);
	}
	public void centerObject(){
		Vertex minX=object.vertices[0];
		Vertex maxX=object.vertices[0];
		Vertex minY=object.vertices[0];
		Vertex maxY=object.vertices[0];
		for(Vertex v:object.vertices){
			if(v.x<minX.x) minX=v;
			if(v.x>maxX.x) maxX=v;
			if(v.y<minY.y) minY=v;
			if(v.y>maxY.y) maxY=v;
		}
		while(((object.projectOnScreen(minY,perspective).y<0&&object.projectOnScreen(maxY,perspective).y<0)?
						Math.abs(object.projectOnScreen(maxY,perspective).y-object.projectOnScreen(minY,perspective).y):
							(object.projectOnScreen(maxY,perspective).y-object.projectOnScreen(minY,perspective).y))<3*getHeight()/4&&
							((object.projectOnScreen(minX,perspective).x<0&&object.projectOnScreen(maxX,perspective).x<0)?
									Math.abs(object.projectOnScreen(maxX,perspective).x-object.projectOnScreen(minX,perspective).x):
										(object.projectOnScreen(maxX,perspective).x-object.projectOnScreen(minX,perspective).x))<3*getWidth()/4){
			object.zoom+=1;
		}
		while(((object.projectOnScreen(minY,perspective).y<0&&object.projectOnScreen(maxY,perspective).y<0)?
						Math.abs(object.projectOnScreen(maxY,perspective).y-object.projectOnScreen(minY,perspective).y):
							(object.projectOnScreen(maxY,perspective).y-object.projectOnScreen(minY,perspective).y))>3*getHeight()/4||
							((object.projectOnScreen(minX,perspective).x<0&&object.projectOnScreen(maxX,perspective).x<0)?
									Math.abs(object.projectOnScreen(maxX,perspective).x-object.projectOnScreen(minX,perspective).x):
										(object.projectOnScreen(maxX,perspective).x-object.projectOnScreen(minX,perspective).x))>3*getWidth()/4){
			object.zoom-=1;
		}
		object.xTranslate=getWidth()/2-(((object.projectOnScreen(minY,perspective).y<0&&object.projectOnScreen(maxY,perspective).y<0)?
				Math.abs(object.projectOnScreen(maxY,perspective).y-object.projectOnScreen(minY,perspective).y):
					(object.projectOnScreen(maxY,perspective).y-object.projectOnScreen(minY,perspective).y)))/2;
		object.yTranslate=getHeight()/2-(((object.projectOnScreen(minX,perspective).x<0&&object.projectOnScreen(maxX,perspective).x<0)?
				Math.abs(object.projectOnScreen(maxX,perspective).x-object.projectOnScreen(minX,perspective).x):
					(object.projectOnScreen(maxX,perspective).x-object.projectOnScreen(minX,perspective).x)))/2;
		//System.out.println(object.xTranslate+" "+object.yTranslate);**/
	}
	public void reset(){
		if(JOptionPane.showOptionDialog(
			    this,
			    "Etes-vous sur de vouloir reinitialiser le modele a son etat initial ?",
			    "Veuillez confirmer votre choix",
			    JOptionPane.YES_NO_OPTION,
			    JOptionPane.QUESTION_MESSAGE,
			    null,
			    new String[]{"Oui, je suis sur de vouloir reinitialiser ce modele","Non, c'est une grave erreur !"},null)==0){
			for(int i=0;i<object.vertices.length;i++){
				object.vertices[i].x=initialObject.vertices[i].x;
				object.vertices[i].y=initialObject.vertices[i].y;
				object.vertices[i].z=initialObject.vertices[i].z;
			}
			for(int i=0;i<object.triangles.length;i++){
				for(int j=0;j<3;j++)object.triangles[i].idxVertices[j]=initialObject.triangles[i].idxVertices[j];
			}
			object.xTranslate=initialObject.xTranslate;
			object.yTranslate=initialObject.yTranslate;
			object.zoom=initialObject.zoom;
			origin=new Point();
			initAxes();
			centerObject();
			object.setVoisins();
			object.setTrianglesShade(lightLayout);
		}
	}
	public void setEye(int value){eye=value;object.eye=value;}
	public void mouseClicked(MouseEvent e){}
	public void mouseEntered(MouseEvent arg0){
		setFocusable(true);
	}
	public void mouseExited(MouseEvent arg0){
		setFocusable(false);
	}
	public void mousePressed(MouseEvent e){
		if(SwingUtilities.isMiddleMouseButton(e)){
			perspective=!perspective;
		}
		if(e.isShiftDown()){
			if(object.selectedVertex1!=null&&object.selectedVertex2!=null)selectedSegmentSize=null;
			object.selectVertex(e.getPoint(), perspective);
			repaint();
			if(object.selectedVertex1!=null&&object.selectedVertex2!=null){
				try{
					selectedSegmentSize=Integer.parseInt(JOptionPane.showInputDialog(this,
							"Entrez la taille du segment selectionne en cm",
							"Taille du segment en cm",
							JOptionPane.INFORMATION_MESSAGE));
				}catch(Exception ex){}
			}
		}
		pressed=e.getPoint();
		repaint();
	}
	public void mouseReleased(MouseEvent e){}
	public void mouseWheelMoved(MouseWheelEvent arg0){
		if(arg0.isControlDown()){
			double[][] rotation={{1.0,     0.0       ,       0.0      },
					 {0.0,Math.cos(arg0.getUnitsToScroll()),-Math.sin(arg0.getUnitsToScroll())},
					 {0.0,Math.sin(arg0.getUnitsToScroll()),Math.cos(arg0.getUnitsToScroll())}};
			axes[0]=Matrix.multiply(axes[0],Matrix.getTransposed(rotation));
			axes[1]=Matrix.multiply(axes[1],Matrix.getTransposed(rotation));
			object.rotateAroundZ(arg0.getUnitsToScroll());
		}else{
			if(object.zoom+arg0.getUnitsToScroll()>0&&object.zoom>1){
				object.zoom+=arg0.getUnitsToScroll();
			}else if(object.zoom+1/((double)arg0.getUnitsToScroll())>0){
				object.zoom+=1/((double)arg0.getUnitsToScroll());
			}
		}
		repaint();
	}
	public void mouseDragged(MouseEvent arg0){
		Point p=arg0.getPoint();
		if(SwingUtilities.isRightMouseButton(arg0)){
			double angleY=-(double)(p.x-pressed.x)/20;
			double angleX=(double)(p.y-pressed.y)/20;
			object.rotateAroundY(angleY);
			object.rotateAroundX(angleX);
			double[][] rotation={{ Math.cos(angleX),0.0,Math.sin(angleX)},
					 {       0.0      ,1.0,      0.0      },
					 {-Math.sin(angleX),0.0,Math.cos(angleX)}};
			axes=Matrix.getTransposed(Matrix.multiply(rotation,Matrix.getTransposed(axes)));
			rotation=new double[][]{{1.0,     0.0       ,       0.0      },
				 {0.0,Math.cos(angleY),-Math.sin(angleY)},
				 {0.0,Math.sin(angleY),Math.cos(angleY)}};
			axes=Matrix.getTransposed(Matrix.multiply(rotation,Matrix.getTransposed(axes)));
		}else{
			object.xTranslate+=(p.x-pressed.x)/100;
			object.yTranslate+=(p.y-pressed.y)/100;
		}
		repaint();
	}
	public void mouseMoved(MouseEvent arg0){}
	public static void main(String[] args){
		//DataBaseInterface.addModele("xwings", "models/x_wing.gts", "cecivole");
		Modele3D m=DataBaseInterface.getModele("head");
		if(m!=null){
			Object3D o=new Object3D("models/head.gts","head");
			System.out.println(o.getWidth()+" "+o.getHeight()+" "+o.getDepth());
			System.out.println("CenterPoint : "+o.get3DCenterPoint());
			new Display(new Renderer(o));
		}
	}
}