package Renderer3d;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;

import bdd.*;

public class bddAccess extends JFrame{

	private JList<String> list;
	private String[] listTab;
	private JPanel pane;
	private JTextArea text;
	private GridBagConstraints c;
	private JButton charge;
	private JButton modify;
	private Renderer rend;
	private JTextField field;
	private JTextField field2;
	
	public bddAccess(Renderer renderer){
		this.rend = renderer;
		int i = 0;
		for(Modele e : DataBaseInterface.getListModele()){
			i++;
		}
		listTab = new String[i];
		i = 0 ;
		for(Modele e : DataBaseInterface.getListModele()){
			listTab[i] = e.getName();
			i++;
		}
		modify = new JButton("Modifier details modele");
		field = new JTextField();
		field2 = new JTextField();
		field.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				int i = 0 ;
				String[] newListTab;
				if(field.getText()==""){
					for(Modele m : DataBaseInterface.getListModele()){
						i++;
					}
					newListTab = new String[i];
					i = 0 ;
					for(Modele m : DataBaseInterface.getListModele()){
						newListTab[i] = m.getName();
						i++;
					}
				}
				else{
					for(Modele m : DataBaseInterface.getListModeleSearchByName(field.getText())){
						i++;
					}
					newListTab = new String[i];
					i=0;
					for(Modele m : DataBaseInterface.getListModeleSearchByName(field.getText())){
						newListTab[i] = m.getName();
						i++;
					}
				}
				list.clearSelection();
				list.setListData(newListTab);
			}
		});
		field2.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				int i = 0 ;
				String[] newListTab;
				if(field.getText()==""){
					for(Modele m : DataBaseInterface.getListModele()){
						i++;
					}
					newListTab = new String[i];
					i = 0 ;
					for(Modele m : DataBaseInterface.getListModele()){
						newListTab[i] = m.getName();
						i++;
					}
				}
				else{
					for(Modele m : DataBaseInterface.getListModeleSearchByTag(field2.getText())){
						i++;
					}
					newListTab = new String[i];
					i=0;
					for(Modele m : DataBaseInterface.getListModeleSearchByTag(field2.getText())){
						newListTab[i] = m.getName();
						i++;
					}
				}
				list.clearSelection();
				list.setListData(newListTab);
			}
		});
		list = new JList<String>(listTab);
		list.addListSelectionListener(new ListSelectionListener(){
			public void valueChanged(ListSelectionEvent e){
				if(list.getSelectedValue()!=null){
					text.setText(DataBaseInterface.getModele(list.getSelectedValue()).getDescription());
				}
			}
		});
		pane = new JPanel();
		text = new JTextArea();
		charge = new JButton("Charger");
		charge.addActionListener(new ActionListener(){   
			public void actionPerformed(ActionEvent arg0) {
				rend.object=new Object3D(DataBaseInterface.getModele(list.getSelectedValue()));
				rend.repaint();
			}
    	});
		modify.addActionListener(new ActionListener(){   
			public void actionPerformed(ActionEvent arg0) {
				if(list.getSelectedValue()!=null){					
					new Modify(DataBaseInterface.getModele(list.getSelectedValue()));
				}
			}
    	});
		text.setEditable(false);
		this.setLayout(new GridBagLayout());
		c = new GridBagConstraints();
		c.fill=1;
		this.add(new JLabel("Recherche par nom:"),c);
		c.weightx=1;
		c.gridx=1;
		this.add(field,c);
		c.gridx=2;
		this.add(new JLabel("Tag: "),c);
		c.gridx=3;
		this.add(field2,c);
		c.gridx=0;
		c.gridy = 1;
		this.add(new JLabel("Nom"),c);
		c.gridx=1;
		c.gridwidth=2;
		this.add(new JLabel("Description"),c);
		c.gridwidth=1;
		c.gridx=3;
		this.add(new JLabel("Apercu"),c);
		c.weighty=1;
		c.gridx=0;
		c.gridy=2;
		c.gridheight=2;
		list.setPreferredSize(new Dimension(200, 500));
		text.setPreferredSize(new Dimension(200, 500));
		pane.setPreferredSize(new Dimension(200, 250));
		this.setSize(600,500);
		this.add(list,c);
		c.gridx=1;
		this.add(text,c);
		c.gridx=3;
		c.gridheight=1;
		this.add(pane,c);
		c.gridy=3;
		c.fill = 0;
		this.add(charge,c);
		c.gridy=4;
		c.gridx=0;
		this.add(modify,c);
		this.setResizable(false);
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.pack();
		this.setVisible(true);
	}
}