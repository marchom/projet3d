package Renderer3d;
public class Matrix {
	public static double getDeterminant(double[][] matrix)throws Exception{
	    if(matrix.length!=matrix[0].length) throw new Exception("La matrice doit être carrée.");
	    if(matrix.length==1) return matrix[0][0];
	    if(matrix.length==2){
	        return matrix[0][0]*matrix[1][1]-matrix[1][0]*matrix[0][1];
	    }
	    double sum = 0.0;
	    for(int i=0;i<matrix[0].length;i++){
	        sum+=(i%2==0?1:-1)*matrix[0][i]*getDeterminant(getSubMatrix(matrix,0,i));
	    }
	    return sum;
	} 
	public static double[][] add(double[][] mat1,double[][] mat2){
		int line,column=0,line2,column2=0;
		double[][] result;
		line = mat1.length;
		if(line>0) column=mat1[0].length;
		line2 = mat1.length;
		if(line2>0) column2=mat2[0].length;
		if(line!=line2 || column!=column2) column=0;line=0;
		result = new double[line][column];
		for (int i=0; i<line;i++){
		      for (int j=0; j<column;j++) result[i][j] = mat1[i][j] + mat2[i][j];
		}
		return result;
	}
	public static double[][] multiply(double[][] mat1, double[][] mat2) {
		int l,c=0,l2,c2=0;
		double[][] result;
		l = mat1.length;
		if(l>0){c=mat1[0].length;}
		l2 = mat1.length;
		if(l2>0){c2=mat2[0].length;}
		if(c!=l2){
			c2=0;
			l=0;
		}
		result = new double[l][c2];
		for(int i=0;i<l;i++){
			for(int j=0;j<c2;j++){
				result[i][j]=0.0;
				for(int k=0;k<c;k++){
					result[i][j]+=mat1[i][k]*mat2[k][j];
				}
			}
		}
		return result;
	}
	public static double[] multiply(double[] mat1, double[][] mat2) {
		double[] result=new double[mat1.length];
		for(int i=0;i<mat1.length;i++){
			for(int j=0;j<mat2.length;j++){
				result[i]+=mat1[j]*mat2[i][j];
			}
		}
		return result;
	}
	public static double[][] multiply(double[][] mat, double fac) {
		for(int i=0;i<mat.length;i++){
			for(int j=0;j<mat[0].length;j++) mat[i][j]=mat[i][j]*fac;
		}
		return mat;
	}
	public static double[][] getTransposed(double[][] matrix) {
		double[][] result = new double[matrix[0].length][matrix.length];
	    for (int i=0;i<matrix.length;i++) {
	        for (int j=0;j<matrix[0].length;j++) {
	            result[j][i]=matrix[i][j];
	        }
	    }
	    return result;
	} 
	public static double[][] getSubMatrix(double[][] mat, int excluding_row, int excluding_col) {
	    double[][] result = new double[mat.length-1][mat[0].length-1];
	    int r = -1;
	    for (int i=0;i<mat.length;i++) {
	        if (i==excluding_row)continue;
	            r++;
	            int c = -1;
	        for(int j=0;j<mat[0].length;j++){
	            if(j==excluding_col)continue;
	            result[r][++c]=mat[i][j];
	        }
	    }
	    return result;
	}
	public static double[][] getCofactor(double[][] matrix)throws Exception{
	    double[][] mat = new double[matrix.length][matrix[0].length];
	    for(int i=0;i<matrix.length;i++){
	        for(int j=0; j<matrix[0].length;j++){
	            mat[i][j]= (i%2==0?1:-1)*(j%2==0?1:-1)*getDeterminant(getSubMatrix(matrix,i,j));
	        }
	    }
	    return mat;
	}
 	public static double[][] reverse(double[][] matrix)throws Exception{
	    return multiply(getTransposed(getCofactor(matrix)),1.0/getDeterminant(matrix));
	} 
	public static String toString(double[][] matrix){
		String result="";
		for(double[] d2:matrix){
			for(double d:d2){
				result+=d+" ";
			}
			result+="\n";
		}
		return result;
	}
	/**
	public static void main(String args[]){
		double[] m1={1.0,2.0,3.0};
		double[][] m2={{1.0,0.0,0.0},
				{0.0,1.0,0.0},
				{0.0,1.0,1.0}};
		double[][] m3={multiply(m1,Matrix.getTransposed(m2))};
		System.out.println(Matrix.toString(m3));	
	}
	**/
}