package Renderer3d;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import bdd.AddForm;
import bdd.DataBaseInterface;
import bdd.Modele;

public class Display extends JFrame implements KeyListener{

	private static final long serialVersionUID = 1L;
	private Renderer rend;
	private Console console;
	private boolean consoleEnabled=true;
	private JFrame frame;
	public Display(Renderer rend){
		frame=this;
		setLayout(new BorderLayout());
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(Toolkit.getDefaultToolkit().getScreenSize());//.width-50,Toolkit.getDefaultToolkit().getScreenSize().height-50);
		setPreferredSize(Toolkit.getDefaultToolkit().getScreenSize());
		setMinimumSize(new Dimension(1024,768));
	    setFocusable(true);
	    addKeyListener(this);
		this.rend = rend;
		rend.setPreferredSize(new Dimension(getWidth(),(int) (getHeight()*(1-Constants.console_height))));
		add(rend,BorderLayout.CENTER);
		console=new Console();
		console.setPreferredSize(new Dimension(getWidth(),(int) (getHeight()*Constants.console_height)));
		add(console,BorderLayout.PAGE_END);
		consoleTrigger();
		setJMenuBar(new MenuBar());
		pack();
		setVisible(true);
	}
	public void reloadMenuBar(){setVisible(false);setJMenuBar(new MenuBar());setVisible(true);}
	public void consoleTrigger(){
		if(console==null){
			console=new Console();
			console.setPreferredSize(new Dimension(getContentPane().getWidth(),
												   (int) (getContentPane().getHeight()*Constants.console_height)));
			getContentPane().add(console,BorderLayout.PAGE_END);
		}else{
			getContentPane().remove(console);
			rend.setSize(getPreferredSize());
			console=null;
		}
		consoleEnabled=!consoleEnabled;
		frame.pack();
	}
	public void keyPressed(KeyEvent arg0){
		boolean test=true;
		if(console!=null){
			if(console.userUsingTheConsole){
				console.pull(arg0);
				test=false;
			}
		}
		if(test){
			double angle=0.1;
			if(System.currentTimeMillis()-rend.lastPressed<250){
				if(rend.intensity<10)rend.intensity+=0.1;
			}else rend.intensity=1/10;
			if(System.currentTimeMillis()-rend.lastPressed>1){
				switch(arg0.getKeyCode()){
					case KeyEvent.VK_PAGE_DOWN:rend.object.rotateAroundX(-angle*rend.intensity*rend.intensity);
					break;
					case KeyEvent.VK_PAGE_UP:rend.object.rotateAroundX(angle*rend.intensity*rend.intensity);
					break;
					case KeyEvent.VK_HOME:rend.object.rotateAroundY(-angle*rend.intensity*rend.intensity);
					break;
					case KeyEvent.VK_END:rend.object.rotateAroundY(angle*rend.intensity*rend.intensity);
					break;
					case KeyEvent.VK_INSERT:rend.object.rotateAroundZ(-angle*rend.intensity*rend.intensity);
					break;
					case KeyEvent.VK_DELETE:rend.object.rotateAroundZ(angle*rend.intensity*rend.intensity);
					break;
					case KeyEvent.VK_LEFT:rend.object.xTranslate-=rend.intensity;
					break;
					case KeyEvent.VK_RIGHT:rend.object.xTranslate+=rend.intensity;
					break;
					case KeyEvent.VK_UP:rend.object.yTranslate-=rend.intensity;
					break;
					case KeyEvent.VK_DOWN:rend.object.yTranslate+=rend.intensity;
					break;
					case KeyEvent.VK_R:rend.reset();
					break;
					case KeyEvent.VK_C:consoleTrigger();
					break;
					case KeyEvent.VK_L:
						rend.lightLayout=!rend.lightLayout;
						rend.repaint();
					break;
					case KeyEvent.VK_P:
						rend.perspective=!rend.perspective;
						rend.repaint();
					break;
					case KeyEvent.VK_T:
						rend.contours=!rend.contours;
						rend.repaint();
					break;
				}
				if((arg0.getKeyCode()==KeyEvent.VK_A))rend.antiAliasing=!rend.antiAliasing;
				rend.repaint();
			}
			rend.lastPressed=System.currentTimeMillis();
		}
	}
	public void keyReleased(KeyEvent arg0){}
	public void keyTyped(KeyEvent arg0){}
	private class MenuBar extends JMenuBar{
		private static final long serialVersionUID = 1L;
		private JMenu effects;
		private JMenu bdd;
		private JMenu menuConsole;
		private JMenu modelsList;
		public MenuBar(){
			loadMenu();
		}
		public void loadMenu(){
		    effects=new JMenu("Effets");
		    bdd=new JMenu("Modeles");
		    menuConsole=new JMenu("Console");
		    this.add(bdd);
		    this.add(effects);
		    this.add(menuConsole);
		    JMenuItem tmp;
		    tmp=new JMenuItem("Importer un fichier gts");
		    tmp.addActionListener(new ActionListener(){   
				public void actionPerformed(ActionEvent arg0) {
					new AddForm(Display.this);
				}
		    });
		    bdd.add(tmp);
		    tmp=new JMenuItem("Ajouter une version pour ce modele");
		    tmp.addActionListener(new ActionListener(){   
				public void actionPerformed(ActionEvent arg0) {
					String name,des;
					boolean exists=false;
					do{
						if(exists) JOptionPane.showMessageDialog(Display.this,"Ce nom existe deja");
						exists=false;
						name=(String)JOptionPane.showInputDialog(Display.this,
							"Entrez un nom pour le modele, ci-dessous :",
							"Ajouter modele",
							JOptionPane.INFORMATION_MESSAGE);
						for(final Modele m:DataBaseInterface.getListModele()) if(m.getName().equals(name)) exists=true;
					}while(exists);
					if(name!=null){
						des=(String)JOptionPane.showInputDialog(Display.this,
								"Entrez une description pour le modele, ci-dessous :",
								"Ajouter modele",
								JOptionPane.INFORMATION_MESSAGE);
							DataBaseInterface.addModele(name, (Object3D)rend.object,des);
							reloadMenuBar();
					}
				}
	    	});
		    bdd.add(tmp);
		    tmp=new JMenuItem("Gestion base de donnee");
		    tmp.addActionListener(new ActionListener(){   
				public void actionPerformed(ActionEvent arg0) {
					new bddAccess(rend);
				}
		    });
		    bdd.add(tmp);
		    modelsList=new JMenu("Charger un modele");
		    for(final Modele m:DataBaseInterface.getListModele()){
		    	tmp=new JMenuItem(m.getName());
		    	tmp.addActionListener(new ActionListener(){   
					public void actionPerformed(ActionEvent arg0) {
						rend.object=new Object3D(DataBaseInterface.getModele(m.getName()));
						rend.repaint();
						rend.initialObject=new Object3D(rend.object);
					}
		    	});
		    	modelsList.add(tmp);
		    }
		    bdd.add(modelsList);
		    tmp=new JMenuItem("Reinitialiser la position de l'objet",KeyEvent.VK_R);
		    tmp.addActionListener(new ActionListener(){   
				public void actionPerformed(ActionEvent arg0) {
					rend.reset();
					rend.repaint();
				}
	    	});
		    effects.add(tmp);
		    JMenu tmp2=new JMenu("Coloration");
		    tmp=new JMenuItem("Generer couleur aleatoire");
		    tmp.addActionListener(new ActionListener(){   
				public void actionPerformed(ActionEvent arg0){
					rend.randomColor=true;
					rend.randColor=new Color(rend.rand.nextInt(255),rend.rand.nextInt(255),rend.rand.nextInt(255));
					rend.repaint();
				}
	    	});
		    tmp2.add(tmp);
		    tmp=new JMenuItem("Reinitialiser");
		    tmp.addActionListener(new ActionListener(){   
				public void actionPerformed(ActionEvent arg0) {
					rend.randomColor=false;
					rend.repaint();
				}
	    	});
		    tmp2.add(tmp);
		    effects.add(tmp2);
		    tmp=new JMenuItem("Tracer contours",KeyEvent.VK_T);
		    tmp.addActionListener(new ActionListener(){   
				public void actionPerformed(ActionEvent arg0) {
					rend.contours=!rend.contours;
					rend.repaint();
				}
	    	});
		    effects.add(tmp);
		    tmp=new JMenuItem("Anti-crenelage",KeyEvent.VK_A);
		    tmp.setSelected(rend.antiAliasing);
		    tmp.addActionListener(new ActionListener(){   
				public void actionPerformed(ActionEvent arg0) {
					rend.antiAliasing=!rend.antiAliasing;
					rend.repaint();
				}
	    	});
		    effects.add(tmp);
		    tmp=new JMenuItem("Centrer l'objet");
		    tmp.addActionListener(new ActionListener(){   
				public void actionPerformed(ActionEvent arg0) {
					rend.centerObject();
					rend.repaint();
				}
	    	});
		    effects.add(tmp);
		    tmp=new JMenuItem("Lissage lumiere",KeyEvent.VK_L);
		    tmp.setSelected(rend.lightLayout);
		    tmp.addActionListener(new ActionListener(){   
				public void actionPerformed(ActionEvent arg0) {
					rend.lightLayout=!rend.lightLayout;
					rend.repaint();
				}
	    	});
		    effects.add(tmp);
		    tmp2=new JMenu("Applatir le modele");
		    tmp=new JMenuItem("Sur le plan X");
		    tmp.addActionListener(new ActionListener(){   
				public void actionPerformed(ActionEvent arg0) {
					if(JOptionPane.showConfirmDialog(
							Display.this,
						    "Etes-vous sur de vouloir applatir le modele sur le plan X ?",
						    "Veuillez confirmer votre choix",
						    JOptionPane.YES_NO_OPTION)==0){
						rend.object.thouShaltFlattenByX();
						rend.repaint();
					}
				}
	    	});
		    tmp2.add(tmp);
		    tmp=new JMenuItem("Sur le plan Y");
		    tmp.addActionListener(new ActionListener(){   
				public void actionPerformed(ActionEvent arg0) {
					if(JOptionPane.showConfirmDialog(
						    Display.this,
						    "Etes-vous sur de vouloir applatir le modele sur le plan Y ?",
						    "Veuillez confirmer votre choix",
						    JOptionPane.YES_NO_OPTION)==0){
						rend.object.thouShaltFlattenByY();
						rend.repaint();
					}
				}
	    	});
		    tmp2.add(tmp);
		    tmp=new JMenuItem("Sur le plan Z");
		    tmp.addActionListener(new ActionListener(){   
				public void actionPerformed(ActionEvent arg0) {
					if(JOptionPane.showConfirmDialog(
							Display.this,
						    "Etes-vous sur de vouloir applatir le modele sur le plan Z ?",
						    "Veuillez confirmer votre choix",
						    JOptionPane.YES_NO_OPTION)==0){
						rend.object.thouShaltFlattenByZ();
						rend.repaint();
					}
				}
	    	});
		    tmp2.add(tmp);
		    effects.add(tmp2);
		    tmp=new JMenuItem("Perspective",KeyEvent.VK_P);
		    tmp.setSelected(false);
		    tmp.addActionListener(new ActionListener(){   
				public void actionPerformed(ActionEvent arg0) {
					rend.perspective=!rend.perspective;
					rend.repaint();
				}
	    	});
		    effects.add(tmp);
		    tmp2=new JMenu("Distorsion du modele");
		    final JSlider js=new JSlider(JSlider.VERTICAL,0,500,500);
		    js.setMajorTickSpacing(50);js.setMinorTickSpacing(50);
			js.setPaintTicks(true);js.setPaintLabels(true);
			js.setForeground(Color.BLACK);js.setOpaque(false);
		    js.addChangeListener(new ChangeListener(){
				public void stateChanged(ChangeEvent arg0) {
					if(js.getValue()>1){
						rend.perspective=true;
						rend.setEye(js.getValue());
						rend.repaint();
					}
				}   
	    	});
		    tmp2.add(js);
		    effects.add(tmp2);
		    tmp=new JMenuItem("Afficher les commandes");
		    tmp.addActionListener(new ActionListener(){   
				public void actionPerformed(ActionEvent arg0) {
					JOptionPane.showMessageDialog(Display.this,
							"help : afficher les commandes de la console\n"+
							"clear : nettoyer la console\n"+
							"flatX : applatir le modele selon l'axe X\n"+
							"flatY : applatir le modele selon l'axe Y\n"+
							"flatZ : applatir le modele selon l'axe Z\n"+
							"reset : remettre le modele a son etat initial\n"+
							"randC : appliquer une couleur aleatoire au modele\n"+
							"reseC : appliquer la couleur de base au modele\n"+
							"aAlia : activer ou pas l'anti-aliasing\n"+
							"persp : activer ou pas la perspective\n"+
							"drawO : afficher ou pas les contours des triangles\n"+
							"light : lisser la luminosite des triangles");
				}
	    	});
		    menuConsole.add(tmp);
		    tmp=new JCheckBoxMenuItem("ON/OFF");
		    tmp.setSelected(consoleEnabled);
		    tmp.addActionListener(new ActionListener(){   
				public void actionPerformed(ActionEvent arg0) {
					consoleTrigger();
				}
	    	});
		    menuConsole.add(tmp);
		}
	}
	private class Console extends JPanel{
		private static final long serialVersionUID = 1L;
		private static final int borderThickness=5;
		public ArrayList<String> content=new ArrayList<String>();
		private String consolePrompt="Console:";
		private FontMetrics fm;
		private int cumul=1;
		private int cursorIdx=0;
		private boolean userUsingTheConsole=false;
		public Console(){
			content.add("");
			addMouseListener(new MouseListener(){
				public void mouseClicked(MouseEvent arg0){}
				public void mouseEntered(MouseEvent arg0){
					userUsingTheConsole=true;
				}
				public void mouseExited(MouseEvent arg0){
					userUsingTheConsole=false;
				}
				public void mousePressed(MouseEvent arg0){}
				public void mouseReleased(MouseEvent arg0){}
			});
		}
		public void paintComponent(Graphics g){
			super.paintComponent(g);
			Graphics2D g2d=(Graphics2D)g;
			fm=g2d.getFontMetrics();
			g2d.setColor(Constants.renderer_background_color);
			g2d.fillRect(0,0,getWidth(),getHeight());
			drawBorder(g2d);
			drawStrings(g2d);
		}
		public void drawBorder(Graphics2D g2d){
			for(int i=0;i<borderThickness;i++){
				g2d.setColor((i%2==0)?Constants.console_border_color_out:Constants.console_border_color_in);
				g2d.drawRect(i,i,getWidth()-(2*i+1),getHeight()-(2*i+1));
			}
		}
		public void drawStrings(Graphics2D g2d){
			g2d.setColor(Constants.console_font_color);
			for(int i=1;i<=(getHeight()-2*borderThickness)/fm.getHeight();i++){
				if(content.size()-i>=0){
					if(content.get(content.size()-i).length()>0||i==1){
						g2d.drawString(consolePrompt+content.get(content.size()-i),
									   borderThickness, 
									   borderThickness+fm.getHeight()*i);
					}
				}
			}
			g2d.drawLine(borderThickness+fm.stringWidth(consolePrompt)+
					fm.stringWidth(content.get(content.size()-1).substring(0,((content.get(content.size()-1).length()-cursorIdx)<0)?0:content.get(content.size()-1).length()-cursorIdx)),
					     borderThickness+fm.getHeight(),
					     borderThickness+fm.stringWidth(consolePrompt)+
							fm.stringWidth(content.get(content.size()-1).substring(0,((content.get(content.size()-1).length()-cursorIdx)<0)?0:content.get(content.size()-1).length()-cursorIdx))+5,
					     borderThickness+fm.getHeight());
		}
		public void pull(KeyEvent arg0){
			switch(arg0.getKeyCode()){
				case KeyEvent.VK_ENTER:console.process(content.get(content.size()-1));
				break;
				case KeyEvent.VK_UP:
					if(content.size()>1){
						content.remove(content.size()-1);
						content.add(content.get(content.size()-cumul));
						cumul+=(content.size()-cumul-2>=0)?1:0;
					}
				break;
				case KeyEvent.VK_DOWN:
					content.remove(content.size()-1);
					if(cumul-1>0){
						cumul--;
						content.add(content.get(content.size()-cumul));
					}else{
						content.add("");
					}
				break;
				case KeyEvent.VK_LEFT:
					if(content.get(content.size()-1).length()>cursorIdx){
						cursorIdx++;
					}
				break;
				case KeyEvent.VK_RIGHT:if(cursorIdx>0)cursorIdx--;
				break;
				case KeyEvent.VK_BACK_SPACE:
					if(content.get(content.size()-1).length()!=0){
						String tmp=content.get(content.size()-1);
						content.remove(content.size()-1);
						content.add(tmp.substring(0,tmp.length()-1));
					}
				break;
				case KeyEvent.VK_DELETE:
					String tmp="";
					boolean lel=false;
					for(int i=0;i<content.get(content.size()-1).length();i++){
						if(i!=content.get(content.size()-1).length()-cursorIdx){
							tmp+=content.get(content.size()-1).charAt(i);
						}else{
							lel=true;
						}
					}
					if(lel)cursorIdx--;
					content.remove(content.size()-1);
					content.add(tmp);
				break;
				default:
					if(!arg0.isActionKey()&&arg0.getKeyCode()!=KeyEvent.VK_SHIFT){
						print(""+arg0.getKeyChar());
					}
				break;
			}
			repaint();
			
		}
		public void process(String s){
			if(s.equals("clear")){
				clear();
			}else if(s.equals("flatX")){
				rend.object.thouShaltFlattenByX();
				rend.repaint();
			}else if(s.equals("flatY")){
				rend.object.thouShaltFlattenByY();
				rend.repaint();
			}else if(s.equals("flatZ")){
				rend.object.thouShaltFlattenByZ();
				rend.repaint();
			}else if(s.equals("reset")){
				rend.reset();
				rend.repaint();
			}else if(s.equals("randC")){
				rend.randomColor=true;
				rend.randColor=new Color(rend.rand.nextInt(255),rend.rand.nextInt(255),rend.rand.nextInt(255));
				rend.repaint();
			}else if(s.equals("reseC")){
				rend.randomColor=false;
				rend.repaint();
			}else if(s.equals("aAlia")){
				rend.antiAliasing=!rend.antiAliasing;
				rend.repaint();
			}else if(s.equals("persp")){
				rend.perspective=!rend.perspective;
				rend.repaint();
			}else if(s.equals("drawO")){
				rend.contours=!rend.contours;
				rend.repaint();
			}else if(s.equals("light")){
				rend.lightLayout=!rend.lightLayout;
				rend.repaint();
			}else if(s.equals("help")){
				JOptionPane.showMessageDialog(Display.this,
					"help : afficher les commandes de la console\n"+
					"clear : nettoyer la console\n"+
					"flatX : applatir le modele selon l'axe X\n"+
					"flatY : applatir le modele selon l'axe Y\n"+
					"flatZ : applatir le modele selon l'axe Z\n"+
					"reset : remettre le modele a son etat initial\n"+
					"randC : appliquer une couleur aleatoire au modele\n"+
					"reseC : appliquer la couleur de base au modele\n"+
					"aAlia : activer ou pas l'anti-aliasing\n"+
					"persp : activer ou pas la perspective\n"+
					"drawO : afficher ou pas les contours des triangles\n"+
					"light : lisser la luminosite des triangles");
			}
			content.add("");
			cursorIdx=0;
		}
		private void clear(){
			for(int i=0;i<(getHeight()-2*borderThickness)/fm.getHeight()-1;i++){
				content.add("");
			}
		}
		public void println(String s){
			content.add(s);
		}
		public void print(String s){
			if(content.size()!=0){
				if(fm.stringWidth(content.get(content.size()-1)+s)<getWidth()-borderThickness*2){
					String tmp=content.get(content.size()-1);
					content.remove(content.size()-1);
					content.add(tmp+s);
				}else{
					content.add(s);
				}
			}else{
				content.add(s);
			}
		}
	}
}
